export class Product{
  productID: number;
  imageURL: string;
  name: string;
  description: string;
  price: number;
}
