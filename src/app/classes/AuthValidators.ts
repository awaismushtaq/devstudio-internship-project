import {AbstractControl, FormControl, ValidatorFn} from "@angular/forms";

// If validation fails, it returns an object, which has key and a value.
// Key contains name of the error and value is always true. If validation passes, it return null
export class AuthValidatorsClass{
  // static passwordValidator(control: AbstractControl): {[key: string]: boolean} | null {
  //   console.log('in passwordValidator');
  //   console.log(control.getError('invalidPassword'));
  //   console.log(control);
  //   let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,12})/;
  //
  //   if(regex.test(control.value)){
  //     // control.get('Password').setErrors({invalidPassword: true});
  //     return {'invalidPassword': true};
  //   }
  //
  //   // control.get('Password').setErrors({invalidPassword: false});
  //   return null;
  // }

  static passwordMatchValidator(control: AbstractControl) {
    const password: string = control.get('Password').value; // get password from our password form control
    const confirmPassword: string = control.get('ConfirmPassword').value; // get password from our confirmPassword form control
    // compare is the password math
    if (password !== confirmPassword) {
      // if they don't match, set an error in our confirmPassword form control
      control.get('ConfirmPassword').setErrors({ NoPassswordMatch: true });
    }
  }

  static updatePasswordMatchValidator(control: AbstractControl) {
    const password: string = control.get('NewPassword').value; // get password from our password form control
    const confirmPassword: string = control.get('ConfirmPassword').value; // get password from our confirmPassword form control
    // compare is the password math
    if (password !== confirmPassword) {
      // if they don't match, set an error in our confirmPassword form control
      control.get('ConfirmPassword').setErrors({ NoPassswordMatch: true });
    }
  }


}
