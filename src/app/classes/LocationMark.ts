// just an interface for type safety
export interface LocationMark {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
