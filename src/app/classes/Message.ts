import {SafeUrl} from '@angular/platform-browser';
import {LocationMark} from "./LocationMark";

export class Message{
  subject: string;
  message: string;
  number: string;
  audioURL: SafeUrl;
  location: LocationMark;
  recipients: string[];
  reasons: string[];
}
