import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterSearch'
})
export class FilterSearchPipe implements PipeTransform {

  transform(productNames: string[], searchText: string): string[] {
    if(!productNames){
      return [];
    }
    if(!searchText){
      return productNames;
    }

    searchText = searchText.toLowerCase();

    return productNames.filter(prod => {
      return prod.toLowerCase().includes(searchText);
    });
  }

}
