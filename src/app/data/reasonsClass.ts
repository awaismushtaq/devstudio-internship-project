import {Reason} from '../classes/Reason';

export class ReasonsClass {
  reasons: Reason[] = [
    {
      reasonID: 1,
      reasonText: 'Work Related.'
    },
    {
      reasonID: 2,
      reasonText: 'Provide Information'
    },
    {
      reasonID: 3,
      reasonText: 'Promotional'
    }
  ];

  constructor() {
  }

  getReasons(): Reason[]{
    return this.reasons;
  }

  addReason(reason: Reason): void {
    this.reasons.push(reason);
  }

  deleteReason(reason: Reason): void{
    const index = this.reasons.findIndex(rsn => rsn.reasonID === reason.reasonID);
    if(index > -1){
      this.reasons.splice(index, 1);
    }
  }
}
