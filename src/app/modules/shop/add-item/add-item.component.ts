import { Component } from '@angular/core';
import {FormBuilder, FormControl, Validators, FormGroup} from "@angular/forms";


@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent{
  myForm: FormGroup;
  title = '';
  imageURL: any;
  points = -1;
  isQuantityLimited = true;
  quantity = -1;
  expiryDate: any;

  isTitleValid = false;
  isPointsValid = false;
  isQuantityValid = false;
  minDate = '';

  constructor(private formBuilder: FormBuilder) {
    this.myForm = formBuilder.group({
      title: [null, Validators.required],
      points: [null, Validators.required],
      quantity: [null, Validators.required],
      expiryDate: [null, Validators.required],
      isQuantityLimited: [true, Validators.required],
      imageURL: [null, Validators.required]
    });

    const currentDay = new Date().getDate();
    let currentDayString = currentDay.toString();
    const currentMonth = new Date().getMonth() + 1; // January is 0
    let currentMonthString = currentMonth.toString();
    const currentYear = new Date().getFullYear();
    const currentYearString = currentYear.toString();

    if (currentDay < 10) {
      currentDayString = '0' + currentDay;
    }
    if (currentMonth < 10) {
      currentMonthString = '0' + currentMonth;
    }
    this.minDate = currentYearString + '-' + currentMonthString + '-' + currentDayString;
  }

  // abstract way of calling FormControl
  get titleValue(): FormControl {
    return this.myForm.get('title') as FormControl;
  }

  get pointsValue(): FormControl {
    return this.myForm.get('points') as FormControl;
  }

  get quantityValue(): FormControl {
    return this.myForm.get('quantity') as FormControl;
  }

  get expiryDateValue(): FormControl {
    return this.myForm.get('expiryDate') as FormControl;
  }

  get imageURLValue(): FormControl {
    return this.myForm.get('imageURL') as FormControl;
  }

  get isQuantityLimitedValue(): FormControl {
    return this.myForm.get('isQuantityLimited') as FormControl;
  }


  readURL(imageInput: HTMLInputElement): void {
    // console.log(imageInput.files);
    if (imageInput.files && imageInput.files[0]) {
      // console.log(imageInput.files[0]);
      const reader = new FileReader();
      reader.readAsDataURL(imageInput.files[0]);
      reader.onload = (e) => {
        this.imageURL = reader.result;
        this.imageURLValue.setValue(this.imageURL);
      };
    }
  }

  showData(formValue: any): void {
    this.title = formValue.title;
    this.points = formValue.points;
    this.quantity = formValue.quantity;
    this.expiryDate = formValue.expiryDate;
    this.isQuantityLimited = formValue.isQuantityLimited;
  }

  validateTitle(): void {
    this.isTitleValid = this.titleValue.value.trim();
  }

  validateQuantity(): void {
    // user shouldn't be able to enter values greater than 100
    this.isQuantityValid = this.quantityValue.value <= 1000;
  }

  isKeyPressValid($event: KeyboardEvent): boolean {
    const keyASCII = $event.key.charCodeAt(0);
    return keyASCII >= 48 && keyASCII <= 57;
  }

  validatePoints(): void {
    // user shouldn't be able to enter values greater than 100
    this.isPointsValid = this.pointsValue.value <= 100;
  }

  validateLimit(): void {
    if (this.isQuantityLimitedValue.value) {
      // do nothing
      this.quantityValue.enable();
    } else {
      this.isQuantityValid = true;
      this.quantityValue.setValue('');

      this.quantityValue.disable();
    }
  }
}
