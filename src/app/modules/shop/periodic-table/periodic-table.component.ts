import {Component, TemplateRef} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, Validators, FormGroup} from "@angular/forms";
import {ToastrService} from "ngx-toastr";

const MYLIST = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

@Component({
  selector: 'app-periodic-table',
  templateUrl: './periodic-table.component.html',
  styleUrls: ['./periodic-table.component.css']
})
export class PeriodicTableComponent {
  title = 'angular-material-table';
  columnsToDisplay: string[] = ['position', 'name', 'weight', 'symbol', 'action'];
  myDataList = MYLIST;
  matTable = new MatTableDataSource(this.myDataList);

  // Form Data
  myForm: FormGroup;
  // isNameValid = false;
  // isSymbolValid = false;
  isWeightValid = false;

  constructor(private modalService: NgbModal, private formBuilder: FormBuilder, private toastr: ToastrService) {
    this.myForm = formBuilder.group({
      name: [null, Validators.required],
      weight: [null, Validators.required],
      symbol: [null, Validators.required]
    });
  }

  showSuccessMessage() {
    this.toastr.success('The element was deleted successfully.', 'Successfully deleted!', {
      timeOut: 3000,
      closeButton: true
    });
  }

  // validateName(): void {
  //   // TODO: User should not enter more than 10 characters (Implemented using HTML)
  //   this.isNameValid = this.myForm.get('name').value.trim();
  // }

  // validateSymbol(): void {
  //   // TODO: User should not be able to enter more than 2 characters (Implemented in HTML)
  //   this.isSymbolValid = this.myForm.get('symbol').value.trim();
  // }

  isKeyPressValid($event: KeyboardEvent): boolean {
    const keyASCII = $event.key.charCodeAt(0);
    return (keyASCII >= 48 && keyASCII <= 57) || keyASCII == 46;
  }

  isCharacterValid($event: KeyboardEvent): boolean {
    const keyASCII = $event.key.charCodeAt(0);
    // console.log(keyASCII);
    return (keyASCII >= 65 && keyASCII <= 90) || (keyASCII >= 97 && keyASCII <= 122);
  }

  validateWeight(): void {
    // TODO: Add checks like the user shouldn't be able to enter negative value or greater than 300
    this.isWeightValid = this.myForm.get('weight').value <= 300;
  }

  // deleteRow(row: any) {
  //   this.myDataList.splice(this.myDataList.indexOf(row), 1);
  //   this.matTable = new MatTableDataSource(this.myDataList);
  //   console.log(this.myDataList);
  //   // Second Method
  //   // this.myDataList = this.myDataList.filter((singleElement)=>{
  //   //   return singleElement.name != row.name;
  //   // })
  //   // console.log(myList);
  //   // console.log(this.myDataList);
  // }

  deleteElement(element: any, confirmDelete: TemplateRef<any>) {
    this.modalService.open(confirmDelete, {ariaLabelledBy: 'modal-basic-title'}).result
      .then((result) => {
        this.myDataList.splice(this.myDataList.indexOf(element), 1);
        this.matTable = new MatTableDataSource(this.myDataList);
        this.showSuccessMessage();
        // console.log(this.myDataList);
        // console.log(result);
      }, (reason) => {
        // this.myForm.reset();
        // this.toastr.error('The element was not deleted!', 'Error in Deletion.');
        // console.log(reason);
      });
  }

  editElement(element: any, content: TemplateRef<any>) {
    this.myForm.get('name').setValue(element.name);
    this.myForm.get('symbol').setValue(element.symbol);
    this.myForm.get('weight').setValue(element.weight);

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result
      .then((result) => {
        // edit element
        // console.log('save button clicked');
        // console.log(result);

        // this.myDataList.splice(this.myDataList.indexOf(row), 1);
        // this.matTable = new MatTableDataSource(this.myDataList);

        for (let i = 0; i < this.myDataList.length; i++) {
          if (this.myDataList[i] === element) {
            // console.log(this.myDataList[i]);
            this.myDataList[i].name = result.name;
            this.myDataList[i].symbol = result.symbol;
            this.myDataList[i].weight = result.weight;
          }
        }

        this.matTable = new MatTableDataSource(this.myDataList);
        // console.log(this.myForm.value);
        this.myForm.reset();
        // console.log(this.myForm.value);
      }, (reason) => {
        this.myForm.reset();
        // console.log(reason);
      });
  }

  addElement(content: TemplateRef<any>) {
    // console.log(this.myDataList);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result
      .then((result) => {
        // get the maximum value in an array using
        // Math.max.apply(Math, array.map(function(o) { return o.y; }))
        this.myDataList.push({
          // position: this.myDataList.length + 1,
          position: Math.max.apply(Math, this.myDataList.map(function (o) {
            return o.position;
          })) + 1,
          name: result.name,
          weight: result.weight,
          symbol: result.symbol
        });
        this.matTable = new MatTableDataSource(this.myDataList);
        this.myForm.reset();
      }, (reason) => {
        // console.log(reason);
        // Reset Error Messages but not the form
        this.myForm.markAsUntouched();
        // if you want to see what happens, enter a value like 10000 and then close and come back
        // and see the results before and after this line
        // this.isWeightValid = true;
      });
  }
}
