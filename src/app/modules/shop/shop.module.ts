import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {ShopRoutingModule} from './shop-routing.module';
import {ShopComponent} from "./shop.component";
import {AddItemComponent} from './add-item/add-item.component';
import {PeriodicTableComponent} from './periodic-table/periodic-table.component';
import {MatTableModule} from '@angular/material/table';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {ToastrModule} from "ngx-toastr";

@NgModule({
  declarations: [
    ShopComponent,
    AddItemComponent,
    PeriodicTableComponent
  ],
  imports: [
    CommonModule,
    ShopRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    MatTableModule,
    ToastrModule.forRoot()
  ],
  exports: [
    ShopComponent,
    AddItemComponent,
    PeriodicTableComponent
  ]
})
export class ShopModule {
}
