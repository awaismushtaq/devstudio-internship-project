import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../services/user.service";
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {AuthValidatorsClass} from "../../../classes/AuthValidators";
import {ToastrModule, ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profileData: any;
  isDataLoading = true;

  isFNameValid = true;
  isLNameValid = true;
  isAddressValid = true;

  isUpdatingProfileData = false;
  isUpdatingPassword = false;

  myForm: FormGroup;
  changePasswordForm: FormGroup;

  constructor(private userService: UserService,
              private formBuilder: FormBuilder,
              private toastr: ToastrService) {
    this.changePasswordForm = formBuilder.group({
      CurrentPassword: [null, [
        Validators.required,
        Validators.maxLength(12),
        Validators.minLength(8),
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,12})/)]],
      NewPassword: [null, [
        Validators.required,
        Validators.maxLength(12),
        Validators.minLength(8),
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,12})/)]],
      ConfirmPassword: [null, [Validators.required, Validators.maxLength(12), Validators.minLength(8)]],
    }, {validator: AuthValidatorsClass.updatePasswordMatchValidator});
  }

  ngOnInit(): void {
    this.userService.getUserData().then((data)=>{
      this.profileData = data;
      this.isDataLoading = false;
      // console.log(this.isDataLoading);
      // console.log('1');
      this.myForm = this.formBuilder.group({
        FName: [this.profileData.FName, [Validators.required, Validators.maxLength(12)]],
        LName: [this.profileData.LName, [Validators.required, Validators.maxLength(12)]],
        Address: [this.profileData.Address, [Validators.required, Validators.maxLength(320)]],
        imageURL: [this.profileData.imageURL, Validators.required]
      });
    }).catch((err)=>{
      this.profileData = null;
      this.myForm = this.formBuilder.group({
        FName: [null],
        LName: [null],
        Address: [null],
        imageURL: [null]
      });
      this.isDataLoading = false;
      // console.log('2');
    });
  }

  get FName(): FormControl{
    return this.myForm.get('FName') as FormControl;
  }

  get LName(): FormControl{
    return this.myForm.get('LName') as FormControl;
  }

  get Address(): FormControl{
    return this.myForm.get('Address') as FormControl;
  }

  get imageURL(): FormControl{
    return this.myForm.get('imageURL') as FormControl;
  }

  get CurrentPassword(): FormControl{
    return this.changePasswordForm.get('CurrentPassword') as FormControl;
  }

  get NewPassword(): FormControl{
    return this.changePasswordForm.get('NewPassword') as FormControl;
  }

  get ConfirmPassword(): FormControl{
    return this.changePasswordForm.get('ConfirmPassword') as FormControl;
  }

  readURL(imageInput: HTMLInputElement): void {
    if (imageInput.files && imageInput.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(imageInput.files[0]);
      reader.onload = (e) => {
        this.imageURL.setValue(reader.result);
        this.myForm.markAsDirty();
      };
    }
  }

  validateFName(): void {
    this.isFNameValid = this.FName.value.trim();
  }

  validateLName(): void {
    this.isLNameValid = this.LName.value.trim();
  }

  validateAddress(): void {
    this.isAddressValid = this.Address.value.trim();
  }

  updateProfileData(formValue: any) {
    this.isUpdatingProfileData = true;
    let updatedData = {
      Title: this.profileData.Title,
      FName: this.FName.value,
      LName: this.LName.value,
      Email: this.profileData.Email,
      Address: this.Address.value,
      imageURL: this.imageURL.value
    };

    // console.log(this.profileData);
    // console.log(updatedData);

    this.userService.updateProfileData(this.profileData, updatedData).then((result) => {
      this.isUpdatingProfileData = false;
      this.profileData = updatedData;
      this.myForm.markAsPristine();
      this.toastr.success('Your data have been successfully updated');
    }).catch((err)=>{
      this.isUpdatingProfileData = false;
      this.toastr.error('Error while updating data.');
    })
    // this.isUpdatingProfileData = false;
  }


  updateProfileDataCancelled() {
    // console.log('hello');
    this.imageURL.setValue(this.profileData.imageURL);
    this.FName.setValue(this.profileData.FName);
    this.LName.setValue(this.profileData.LName);
    this.Address.setValue(this.profileData.Address);

    // Opposite of Dirty
    this.myForm.markAsPristine();
  }

  updatePasswordCancelled() {
    this.CurrentPassword.setValue(null);
    this.NewPassword.setValue(null);
    this.ConfirmPassword.setValue(null);
  }

  updatePassword() {
    this.isUpdatingPassword = true;

    this.userService.updatePassword(this.CurrentPassword.value, this.NewPassword.value).then((result) => {
      this.isUpdatingPassword = false;
      this.changePasswordForm.reset();
      this.toastr.success('Your password have been successfully updated');
    }).catch((err)=>{
      this.isUpdatingPassword = false;
      this.toastr.error(err);
    })

  }
}
