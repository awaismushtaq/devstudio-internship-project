import {Component, OnInit, TemplateRef} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {MatTableDataSource} from "@angular/material/table";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  userOrders: any[] = [];
  isDataLoading = true;

  columnsToDisplay: string[] = ['order-id', 'order-time', 'order-total', 'order-status', 'action'];
  matTable;

  selectedOrder: any;

  constructor(private userService: UserService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.userService.getOrderHistory().then((orders)=>{
      // console.log('1');
      this.userOrders = orders;
      // console.log(this.userOrders);
      // sort in desc order
      this.userOrders.sort(function(a, b){return b.orderID-a.orderID});
      // console.log(this.userOrders);
      this.matTable = new MatTableDataSource(this.userOrders);
      this.isDataLoading = false;
    }).catch((error) => {
      // console.log('2');
      this.matTable = new MatTableDataSource(this.userOrders);
      this.isDataLoading = false;
    });
  }

  showDetails(showDetailsModal: TemplateRef<any>, order: any) {
    this.userService.getOrderDetails(order).then((snapshot) => {
      this.selectedOrder = snapshot.val();

      this.modalService.open(showDetailsModal, {ariaLabelledBy: 'modal-basic-title'})
        .result.then((result)=>{
        // do nothing
      }).catch((error)=>{
        // console.log(error);
      });
    }).catch((error) => {
      // console.log(error);
    });
  }

}
