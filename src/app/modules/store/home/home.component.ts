import {Component, OnInit} from '@angular/core';
import {AllProductsService} from "../../../services/all-products.service";
import {Product} from "../../../classes/Product";
import {CartService} from "../../../services/cart.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  allProducts: Product[] = [];
  isDataLoading = true;

  searchText: string;
  productSearchData: string[] = [];
  isShowingSearchResult = false;
  isMouseOnSearchResults = false;
  isSearchBoxFocused = false;
  searchResultProducts: Product[] = [];

  isCardHovered: boolean = false;
  hoveredItemNum: number = -1;
  toShowCart = false;
  productsInCart = [];



  constructor(private allProductsService: AllProductsService,
              private cartService: CartService,
              private toastr: ToastrService) {
    // console.log('in home component constructor');
  }

  ngOnInit(): void {
    this.allProductsService.getInitialData().then((products) => {
      this.allProducts = products;
      for (let i = 0; i < this.allProducts.length; i++){
        this.productSearchData.push(this.allProducts[i].name);
      }
      this.isDataLoading = false;
    }).catch((error) => {
      // console.log(error);
    });
    this.productsInCart = this.cartService.getProducts();
    this.cartService.noOfItems.subscribe(noOfItems =>{
      this.productsInCart = this.cartService.getProducts();
    });
  }

  // addToCart(i: number) {
  //   this.cartService.addProduct(this.allProducts[i]);
  //   this.toastr.success(this.allProducts[i].name + ' added to the cart. ');
  //   this.toShowCart = true;
  //   // this.productsAddedToTheCart.push(i);
  //   this.productsInCart = this.cartService.getProducts();
  // }

  addToCart(product: Product) {
    this.cartService.addProduct(product);
    this.toastr.success(product.name + ' added to the cart. ');
    this.toShowCart = true;
    // this.productsAddedToTheCart.push(i);
    this.productsInCart = this.cartService.getProducts();
  }

  isProductInCart(product: Product): boolean {
    const checkIfExists = cartProduct => cartProduct.product.productID === product.productID;
    return this.productsInCart.some(checkIfExists);

    // productsInCart.findIndex(prod => prod.product.productID === product.productID
  }

  findProductInCart(product: Product) {
    const foundIndex = this.productsInCart.findIndex(prod => prod.product.productID === product.productID);
    return this.productsInCart[foundIndex];
  }

  incrementQuantity(product: Product) {
    if(this.cartService.incrementQuantity(product)){
      this.toastr.info('Increased quantity of ' + product.name + ' by 1');
    }
    else{
      this.toastr.error('Max length reached!');
    }
  }

  decrementQuantity(product: Product) {
    if(this.cartService.decrementQuantity(product)){
      this.toastr.info('Decreased quantity of ' + product.name + ' by 1');
    }
    else{
      this.toastr.error('Min length reached!');
    }
  }

  removeFromCart(product: Product) {
    this.toastr.info(product.name + ' removed successfully!');
    this.cartService.removeProduct(product);
  }

  showClickedProduct(data: string) {
    // console.log('clicked');
    // this.isItemClicked = true;
    this.searchText = null;
    this.isShowingSearchResult = true;
    this.searchResultProducts = this.allProducts.filter((prod)=>{
      return prod.name === data;
    });
    // console.log(this.searchResultProducts);
  }

  showSearchResult(searchText: string) {
    if(searchText){
      this.isShowingSearchResult = true;
      this.searchResultProducts = this.allProducts.filter((prod)=>{
        return prod.name.toLowerCase().includes(searchText.toLowerCase());
      });
      this.searchText = null;
    }
  }

  goBack() {
    this.isShowingSearchResult = false;
    this.searchResultProducts = []
  }
}
