import {Component, OnInit} from '@angular/core';
import {CartService} from "../../services/cart.service";

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styles: []
})
export class StoreComponent implements OnInit{
  active = 'home';
  noOfItemsInCart: number;

  constructor(private cartService: CartService) {
    // console.log('in store component constructor');
  }

  ngOnInit(): void {
    this.cartService.noOfItems.subscribe((noOfItems)=>{
      this.noOfItemsInCart = noOfItems;
    });
  }
}
