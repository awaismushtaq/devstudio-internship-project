import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreRoutingModule} from "./store-routing.module";
import {StoreComponent} from "./store.component";
import {HomeComponent} from './home/home.component';
import {ShopComponent} from './shop/shop.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {CartComponent} from './cart/cart.component';
import {CartService} from "../../services/cart.service";
import {ToastrModule} from "ngx-toastr";
import {AdminService} from "../../services/admin.service";
import {NgxLoadersCssModule} from "ngx-loaders-css";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FilterSearchPipe} from "../../pipes/filter-search.pipe";
import {HistoryComponent} from './history/history.component';
import {MatTableModule} from "@angular/material/table";
import {ProfileComponent} from './profile/profile.component';
import {UserService} from "../../services/user.service";

@NgModule({
  declarations: [
    StoreComponent,
    HomeComponent,
    ShopComponent,
    CartComponent,
    HistoryComponent,
    FilterSearchPipe,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    StoreRoutingModule,
    NgbModule,
    ToastrModule,
    NgxLoadersCssModule,
    FormsModule,
    MatTableModule,
    ReactiveFormsModule
  ],
  exports: [
    StoreComponent,
    HomeComponent,
    ShopComponent,
    CartComponent,
    HistoryComponent,
    ProfileComponent
  ],
  providers: [
    CartService,
    AdminService,
    FilterSearchPipe,
    UserService
  ]
})
export class StoreModule {
}
