import { Component, OnInit } from '@angular/core';
import {CartService} from "../../../services/cart.service";
import {Product} from "../../../classes/Product";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cartProducts: { product: Product, quantity: number }[] = [];
  cartTotal = 0;
  cartSubtotal = 0;
  cartTax = 0;

  constructor(private cartService: CartService,
              private toastr: ToastrService) { }

  ngOnInit(): void {
    this.cartProducts = this.cartService.getProducts();
    this.cartService.noOfItems.subscribe(noOfItems =>{
      this.cartProducts = this.cartService.getProducts();
    });
    this.cartService.cartTotal.subscribe((total)=>{
      this.cartTotal = total;
    });
    this.cartService.cartTax.subscribe((tax)=>{
      this.cartTax = tax * 100;
    });
    this.cartService.cartSubtotal.subscribe((subtotal)=>{
      this.cartSubtotal = subtotal
    });

  }

  removeFromCart(i: number) {
    this.toastr.info(this.cartProducts[i].product.name + ' removed successfully!');
    this.cartService.removeProduct(this.cartProducts[i].product);
  }

  incrementQuantity(i: number) {
    if(this.cartService.incrementQuantity(this.cartProducts[i].product)){
      this.toastr.info('Increased quantity of ' + this.cartProducts[i].product.name + ' by 1');
    }
    else{
      this.toastr.error('Max length reached!');
    }
  }

  decrementQuantity(i: number){
    if(this.cartService.decrementQuantity(this.cartProducts[i].product)){
      this.toastr.info('Decreased quantity of ' + this.cartProducts[i].product.name + ' by 1');
    }
    else{
      this.toastr.error('Min length reached!');
    }
  }

  checkout() {
    this.cartService.checkout().then((result)=>{
      this.toastr.success(result);
    }).catch((error)=>{
      this.toastr.error('Error in placing your order!');
    })
  }
}
