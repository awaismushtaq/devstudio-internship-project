import { Component } from '@angular/core';

@Component({
  selector: 'app-api-requests',
  templateUrl: './api-requests.component.html',
  styles: []
})
export class ApiRequestsComponent {
  title = 'api-requests-module';
  active = '1';
}
