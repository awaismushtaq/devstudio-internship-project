import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumsApiComponent } from './albums-api.component';

describe('AlbumsApiComponent', () => {
  let component: AlbumsApiComponent;
  let fixture: ComponentFixture<AlbumsApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumsApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumsApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
