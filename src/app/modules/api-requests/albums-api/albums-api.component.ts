import { Component, OnInit } from '@angular/core';

import {Albums} from "../../../classes/Albums";
import {Photos} from "../../../classes/Photos";

import {APIURLs} from "../../../../environments/APIs/APIURLS";

import {HttpParams} from "@angular/common/http";
import {HttpService} from "../../../services/http.service";


@Component({
  selector: 'app-albums-api',
  templateUrl: './albums-api.component.html',
  styleUrls: ['./albums-api.component.css']
})
export class AlbumsApiComponent implements OnInit {

  albumsList: Albums[];
  selectedAlbum: number = null;  // will contain the id of the selected album
  photosList: Photos[];

  constructor(private httpService: HttpService) {}

  ngOnInit(): void {
    this.httpService.get(APIURLs.albumsAPI).subscribe(data =>{
      // console.log('data ' + data);
      // console.log(data[0]);
      this.albumsList = data;
      // console.log(this.albumsList);
    });
    // console.log(this.albumsList);
  }

  onAlbumSelected(selectedAlbumID: any): void {
    let param1 = new HttpParams().set('albumId', selectedAlbumID);
    this.httpService.getWithParams(APIURLs.photosAPI, param1).subscribe(data =>{
      this.photosList = data;
    });
  }
}
