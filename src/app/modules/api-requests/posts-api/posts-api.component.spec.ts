import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsApiComponent } from './posts-api.component';

describe('PostsApiComponent', () => {
  let component: PostsApiComponent;
  let fixture: ComponentFixture<PostsApiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsApiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsApiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
