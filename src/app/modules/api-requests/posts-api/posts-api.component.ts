import { Component, OnInit } from '@angular/core';

import {Posts} from "../../../classes/Posts";
import {Comments} from "../../../classes/Comments";

import {APIURLs} from "../../../../environments/APIs/APIURLS";

import {HttpService} from "../../../services/http.service";
import {HttpParams} from "@angular/common/http";


@Component({
  selector: 'app-posts-api',
  templateUrl: './posts-api.component.html',
  styleUrls: ['./posts-api.component.css']
})
export class PostsApiComponent implements OnInit {

  commentsList: Comments[];
  postsList: Posts[];
  selectedUserID: number;
  wasDataFetched: boolean;
  showErrorMessage: boolean;
  newPost: Posts;
  newPutObject: Posts;
  newPatchObject: Posts;
  newDeleteMessage: string;

  constructor(private httpService: HttpService) {}

  ngOnInit(): void {
    this.httpService.get(APIURLs.commentsAPI).subscribe(data => {
      this.commentsList = data;
    });

    let myPost = new Posts();
    myPost.body = 'testBody';
    myPost.title = 'testTitle';
    myPost.userId = 5;

    this.httpService.post(APIURLs.postsAPIPost, myPost).subscribe(data => {
      this.newPost = data;
    });

    myPost = new Posts();
    myPost.body = 'updated body';
    myPost.title = 'updated title';
    myPost.userId = 5;

    this.httpService.put(APIURLs.postsAPIManipulation, myPost).subscribe(data => {
      this.newPutObject = data;
    });

    // Difference between put and patch is that put method updates the resource and returns a new
    // copy while patch method updates the existing copy of the resource
    myPost = new Posts();
    myPost.title = 'Patched Title';

    this.httpService.patch(APIURLs.postsAPIManipulation, myPost).subscribe(data => {
      this.newPatchObject = data;
    });

    this.httpService.delete(APIURLs.postsAPIManipulation).subscribe(data => {
      this.newDeleteMessage = 'Resource deleted Successfully.';
    });
  }

  isKeyPressValid($event: KeyboardEvent): boolean {
    const keyASCII = $event.key.charCodeAt(0);
    return keyASCII >= 48 && keyASCII <= 57;
  }

  fetchUserPosts() {
    let param1 = new HttpParams().set('userId', this.selectedUserID.toString());
    this.httpService.getWithParams(APIURLs.commentsAPIByParams, param1).subscribe(data =>{
      if(data && data.length > 0){
        // console.log('14');
        this.postsList = data;
        this.wasDataFetched = true;
        this.showErrorMessage = false;
      }
      else{
        this.wasDataFetched = false;
        this.showErrorMessage = true;
      }
    });
  }
}
