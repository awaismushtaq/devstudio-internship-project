import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ApiRequestsComponent} from "./api-requests.component";

const routes: Routes = [
  {
    path: '',
    component: ApiRequestsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApiRequestsRoutingModule {
}
