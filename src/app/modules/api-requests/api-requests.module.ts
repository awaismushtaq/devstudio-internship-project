import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {ApiRequestsComponent} from "./api-requests.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {ApiRequestsRoutingModule} from "./api-requests-routing.module";
import { PostsApiComponent } from './posts-api/posts-api.component';
import { AlbumsApiComponent } from './albums-api/albums-api.component';

@NgModule({
  declarations: [
    ApiRequestsComponent,
    PostsApiComponent,
    AlbumsApiComponent
  ],
  imports: [
    CommonModule,
    ApiRequestsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
  ],
  exports: [
    ApiRequestsComponent,
    PostsApiComponent,
    AlbumsApiComponent
  ]
})
export class ApiRequestsModule { }
