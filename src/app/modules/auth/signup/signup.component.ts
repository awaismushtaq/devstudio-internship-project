import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {AuthValidatorsClass} from "../../../classes/AuthValidators";
import {ChangeActiveService} from "../../../services/change-active.service";
import {AuthService} from "../../../services/auth.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SignupComponent implements OnInit {

  loaderColor = 'whitesmoke';

  myForm: FormGroup;
  isSigningUp = false;

  titleList = ['Mr.', 'Mrs.', 'Others'];

  toShowSignupMessage = false;
  isFNameValid = true;
  isLNameValid = true;
  isAddressValid = true;

  constructor(private formBuilder: FormBuilder,
              private changeActiveService: ChangeActiveService,
              private authService: AuthService,
              private toastr: ToastrService) {
    this.myForm = formBuilder.group({
      Title: [null, Validators.required],
      FName: [null, [Validators.required, Validators.maxLength(12)]],
      LName: [null, [Validators.required, Validators.maxLength(12)]],
      Email: [null, [Validators.required, Validators.email]],
      Password: [null, [
        Validators.required,
        Validators.maxLength(12),
        Validators.minLength(8),
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,12})/)]],
      ConfirmPassword: [null, [Validators.required, Validators.maxLength(12), Validators.minLength(8)]],
      Address: [null, [Validators.required, Validators.maxLength(320)]],
      imageURL: [null, Validators.required]
    }, {validator: AuthValidatorsClass.passwordMatchValidator});

    // console.log('loading singup.component.ts..');
  }

  get Title(): FormControl {
    return this.myForm.get('Title') as FormControl;
  }

  get FName(): FormControl {
    return this.myForm.get('FName') as FormControl;
  }

  get LName(): FormControl {
    return this.myForm.get('LName') as FormControl;
  }

  get Email(): FormControl {
    return this.myForm.get('Email') as FormControl;
  }

  get Password(): FormControl {
    return this.myForm.get('Password') as FormControl;
  }

  get ConfirmPassword(): FormControl {
    return this.myForm.get('ConfirmPassword') as FormControl;
  }

  get Address(): FormControl {
    return this.myForm.get('Address') as FormControl;
  }

  get imageURL(): FormControl{
    return this.myForm.get('imageURL') as FormControl;
  }

  ngOnInit(): void {
  }

  formChanged() {
    this.toShowSignupMessage = false;
  }

  validateFName(): void {
    this.isFNameValid = this.FName.value.trim();
  }

  validateLName(): void {
    this.isLNameValid = this.LName.value.trim();
  }

  validateAddress(): void {
    this.isAddressValid = this.Address.value.trim();
  }

  signup(value: FormGroup): void {
    this.isSigningUp = true;
    this.authService.SignUp(value).then((result) => {
      this.isSigningUp = false;
      this.myForm.reset();
      this.toastr.success('You have successfully Signed Up.', 'Signup Successful');
    }).catch((error) => {
      // console.log('hello');
      this.isSigningUp = false;
      this.toastr.error(error, 'Signup Error')
    });
  }

  goToLoginPage(): void {
    this.myForm.reset();
    this.changeActiveService.activeTab.next('login');
  }

  readURL(imageInput: HTMLInputElement): void {
    if (imageInput.files && imageInput.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(imageInput.files[0]);
      reader.onload = (e) => {
        this.imageURL.setValue(reader.result);
      };
    }
  }

}
