import {Component, OnDestroy, OnInit} from '@angular/core';
import {ChangeActiveService} from "../../services/change-active.service";
import {AuthService} from "../../services/auth.service";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-authentication',
  templateUrl: './auth.component.html',
  styles: []
})
export class AuthComponent implements OnInit {
  title = 'course-module';
  active = 'login';
  isUserLoggedIn = false;
  isAdminLoggedIn = false;

  constructor(private changeActiveService: ChangeActiveService,
              private authService: AuthService,
              private router: Router) {
    // console.log('in auth component constructor');
  }

  subscription: Subscription;

  ngOnInit(): void {
    this.changeActiveService.activeTab.subscribe((res: string) => this.active = res);
    this.subscription = this.authService.isAdminLoggedIn.subscribe((res: boolean) => {
      this.isAdminLoggedIn = res;
      if (res) {
        // Go To Admin Panel
        this.router.navigate(['admin']);
        // this.active = 'home';
      }
      else if(!this.isUserLoggedIn){
        this.active = 'admin-login';
      }
    });
    this.subscription = this.authService.isUserLoggedIn.subscribe((res: boolean) => {
      this.isUserLoggedIn = res;
      if (res && !this.isAdminLoggedIn) {
        // Go To Store
        this.router.navigate(['store']);

        // this.active = 'home'
      }
      else if(!this.isAdminLoggedIn) {
        this.active = 'login';
      }
    });
    this.authService.publishLoginStatus();
  }
  //
  // ngOnDestroy(): void {
  //   this.subscription.unsubscribe();
  // }
}
