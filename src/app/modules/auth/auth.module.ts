import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthComponent} from "./auth.component";
import {AuthRoutingModule} from "./auth-routing.module";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {NgSelectModule} from "@ng-select/ng-select";
import {ChangeActiveService} from "../../services/change-active.service";
import {AngularFireModule} from "@angular/fire";
import {environment} from "../../../environments/environment";
import {AngularFireDatabaseModule} from "@angular/fire/database";
import {AdminLoginComponent} from './admin-login/admin-login.component';
import {NgxLoadersCssModule} from "ngx-loaders-css";

@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    SignupComponent,
    AdminLoginComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireModule,
    AngularFireDatabaseModule,
    NgxLoadersCssModule
  ],
  exports: [
    AuthComponent,
    LoginComponent,
    SignupComponent,
    AdminLoginComponent
  ],
  providers: [
    ChangeActiveService
  ]
})
export class AuthModule {
}
