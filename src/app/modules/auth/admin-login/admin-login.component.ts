import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ChangeActiveService} from "../../../services/change-active.service";
import {AngularFireAuth} from "@angular/fire/auth";
import {AuthService} from "../../../services/auth.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  loaderColor = 'whitesmoke';

  myForm: FormGroup;
  isLoggingIn = false;

  toShowLoginMessage = false;

  constructor(private formBuilder: FormBuilder, private changeActiveService: ChangeActiveService,
              private afAuth: AngularFireAuth,
              private authService: AuthService,
              private toastr: ToastrService) {
    this.myForm = formBuilder.group({
      Email: [null, [Validators.required, Validators.email]],
      Password: [null, [
        Validators.required,
        Validators.maxLength(12),
        Validators.minLength(8)]]
    });

    // console.log('loading admin-login.component.ts..');
  }

  get Email(): FormControl {
    return this.myForm.get('Email') as FormControl;
  }

  get Password(): FormControl {
    return this.myForm.get('Password') as FormControl;
  }

  ngOnInit(): void {
  }

  formChanged() {
    this.toShowLoginMessage = false;
  }

  adminLogin(value: any): void {
    this.isLoggingIn = true;
    this.authService.AdminLogin(value).then((result) => {
      this.isLoggingIn = false;
      this.toastr.success('You have successfully logged in.', 'Successful Login')
      this.myForm.reset();
    }).catch((error) => {
      this.isLoggingIn = false;
      this.toastr.error(error.message, 'Login Error');
    })
  }

  goToSignupPage(): void {
    this.myForm.reset();
    this.changeActiveService.activeTab.next('signup');
  }

}
