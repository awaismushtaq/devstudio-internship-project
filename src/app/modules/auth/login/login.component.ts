import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ChangeActiveService} from "../../../services/change-active.service";
import {AngularFireAuth} from "@angular/fire/auth";
import {AuthService} from "../../../services/auth.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loaderColor = 'whitesmoke';

  myForm: FormGroup;

  toShowLoginMessage = false;

  isLoggingIn = false;

  constructor(private formBuilder: FormBuilder, private changeActiveService: ChangeActiveService,
              private afAuth: AngularFireAuth,
              private authService: AuthService,
              private toastr: ToastrService) {
    this.myForm = formBuilder.group({
      Email: [null, [Validators.required, Validators.email]],
      Password: [null, [
        Validators.required,
        Validators.maxLength(12),
        Validators.minLength(8)]]
    });

    // console.log('loading login.component.ts..');
  }

  get Email(): FormControl {
    return this.myForm.get('Email') as FormControl;
  }


  get Password(): FormControl {
    return this.myForm.get('Password') as FormControl;
  }

  ngOnInit(): void {
  }

  formChanged() {
    this.toShowLoginMessage = false;
  }


  login(value: any): void {
    this.isLoggingIn = true;
    this.authService.LogIn(value).then((result) => {
      this.toastr.success('You have successfully logged in.', 'Successful Login')
      this.myForm.reset();
      this.isLoggingIn = false;
    }).catch((error) => {
      this.toastr.error(error.message, 'Login Error');
      this.isLoggingIn = false;
    })
  }


  goToSignupPage(): void {
    this.myForm.reset();
    this.changeActiveService.activeTab.next('signup');
  }
}
