import {Component, OnInit} from '@angular/core';

import {FormBuilder, FormControl, Validators, FormGroup} from '@angular/forms';

import {ReasonsClass} from "../../../data/reasonsClass";
import {Reason} from "../../../classes/Reason";
import {OptionsObject} from "../../../data/options";
import {AllMessages} from "../../../data/all-messages";

import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.css']
})
export class OptionsComponent implements OnInit {
  myForm: FormGroup;
  isReasonValid = false;
  reasonsList: Reason[] = [];
  toShowMessage = false;
  hasFormChanged = false;

  constructor(private formBuilder: FormBuilder, private reasonsClass: ReasonsClass,
              private modalService: NgbModal) {
    this.myForm = formBuilder.group({
      toTakeMessage: [OptionsObject.toTakeMessage, Validators.required],
      toTakeSubject: [OptionsObject.toTakeSubject, Validators.required],
      reason: [null]
    });

    this.reasonsList = reasonsClass.getReasons();
  }

  private get reasonValue(): FormControl {
    return this.myForm.get('reason') as FormControl;
  }

  ngOnInit(): void {
  }

  saveOptions(value: any): void {
    // Save Globally
    OptionsObject.toTakeMessage = value.toTakeMessage;
    OptionsObject.toTakeSubject = value.toTakeSubject;
    OptionsObject.reasonsList = this.reasonsList;
    this.toShowMessage = true;
    this.hasFormChanged = false;
  }

  validateReason(): void {
    this.isReasonValid = this.reasonValue.value.trim();
  }

  addReason(value: any): void {
    const newID = Math.max.apply(Math, this.reasonsList.map(reason => reason.reasonID)) + 1;
    this.reasonsClass.addReason({reasonID: newID, reasonText: value});
    this.reasonsList = this.reasonsClass.getReasons();
    this.reasonValue.setValue('');
    this.reasonValue.markAsUntouched();
  }

  // deleteReason(reason: Reason): void {
  //   // console.log('inside deleteReason')
  //   // console.log(reason);
  //   // console.log(AllMessages.some(msg => msg.reasons));
  //   const found = AllMessages.some(msg => msg.reasons.includes(reason.reasonText));
  //   let toDelete = true;
  //   if(found){
  //     this.toConfirmDeletion = true;
  //     // console.log(toDelete);
  //
  //   }

  deleteReason(content, reason): void {
    // console.log('inside deleteReason');
    // console.log(reason);
    // console.log(AllMessages);
    // console.log(AllMessages.some(msg => msg.reasons));
    const found = AllMessages.some(msg => msg.reasons.includes(reason.reasonText));
    if (!found) {
      // console.log('hello');
      this.reasonsClass.deleteReason(reason);
      this.reasonsList = this.reasonsClass.getReasons();
      this.formChanged();
    }
    else{
      this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})
        .result.then((result) => {
          if (result === 'Confirmed'){
            this.reasonsClass.deleteReason(reason);
            this.reasonsList = this.reasonsClass.getReasons();
            this.formChanged();
          }
          else if (result === 'Delete Messages'){
            // Delete this reason
            this.reasonsClass.deleteReason(reason);
            this.reasonsList = this.reasonsClass.getReasons();

            // delete messages belonging to this reason
            // const forLoopLimit = AllMessages.length;
            for (let i = 0; i < AllMessages.length; i++){
              // console.log('1');
              // console.log(AllMessages.length);
              // console.log(i);
              // console.log(reason.reasonText);
              // console.log(AllMessages[i].reasons);
              const index = AllMessages[i].reasons.findIndex(rsn => rsn === reason.reasonText);
              // console.log(i + ' : ' + index);
              if (index > -1){
                // console.log('h');
                // console.log(i);
                // console.log(index);
                AllMessages.splice(i, 1);
                // after the deletion, AllMessages length decreases and i should start from the previous index
                i--;
              }
            }
            this.formChanged();
          }
      }, (reason) => {
        // console.log('closed');
      });
    }
  }

  formChanged(): void {
    this.hasFormChanged = true;
    this.toShowMessage = false;
  }
}
