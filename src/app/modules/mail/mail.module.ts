import {NgModule, Output} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MailComponent} from "./mail.component";
import {MailRoutingModule} from "./mail-routing.module";

import {InterfaceComponent} from "./interface/interface.component";
import {OptionsComponent} from "./options/options.component";
import {OutputComponent} from "./output/output.component";

import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {MatTableModule} from '@angular/material/table';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxMaskModule} from 'ngx-mask';

import {UsersListService} from "../../services/users-list.service";
import {ReasonsClass} from "../../data/reasonsClass";
import {AgmCoreModule} from "@agm/core";
import {FormsArrayComponent} from './forms-array/forms-array.component';
import {NgSelectModule} from "@ng-select/ng-select";

@NgModule({
  declarations: [
    MailComponent,
    InterfaceComponent,
    OutputComponent,
    OptionsComponent,
    FormsArrayComponent
  ],
  imports: [
    CommonModule,
    MailRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule,
    NgSelectModule,
    MatTableModule,
    NgbModule,
    NgxMaskModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBjET7vufGHnYur2Tg1jneuHbcrY4ejpOc'
    })
  ],
  exports: [
    MailComponent,
    InterfaceComponent,
    OptionsComponent,
    OutputComponent,
    FormsArrayComponent
  ],
  providers: [
    UsersListService,
    ReasonsClass
  ]
})
export class MailModule {
}

// platformBrowserDynamic().bootstrapModule(MailModule);
