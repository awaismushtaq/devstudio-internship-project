import {Component, TemplateRef} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

import {AllMessages} from "../../../data/all-messages";

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent {
  columnsToDisplay: string[] = ['subject', 'message', 'number', 'audio', 'location', 'recipients', 'reasons', 'action'];
  matTable = new MatTableDataSource(AllMessages);

  constructor(private modalService: NgbModal) {
  }

  deleteMessage(element: any, confirmDelete: TemplateRef<any>): void {
    this.modalService.open(confirmDelete, {ariaLabelledBy: 'modal-basic-title'}).result
      .then((result) => {
        // console.log(result);
        // delete the message
        const msgIndex = AllMessages.findIndex(e => e.audioURL === element.audioURL);
        if (msgIndex > -1){
          AllMessages.splice(msgIndex, 1);
          this.matTable = new MatTableDataSource(AllMessages);
        }
        // console.log(msgIndex);
        // console.log(AllMessages);
        // console.log(element);
      }, (reason) => {
        // console.log('deletion cancelled');
      });
  }
}
