import {ChangeDetectorRef, Component, OnInit, TemplateRef} from '@angular/core';

import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';

import {IDropdownSettings} from 'ng-multiselect-dropdown';

import {UsersListService} from "../../../services/users-list.service";

import {OptionsObject} from "../../../data/options";
import {Message} from "../../../classes/Message";
import {AllMessages} from "../../../data/all-messages";
import {ReasonsClass} from "../../../data/reasonsClass";

import * as RecordRTC from 'recordrtc';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';

import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

import {MouseEvent as AgmMouseEvent} from "@agm/core";
import {LocationMark} from "../../../classes/LocationMark";

@Component({
  selector: 'app-interface',
  templateUrl: './interface.component.html',
  styleUrls: ['./interface.component.css']
})
export class InterfaceComponent implements OnInit {

  myForm: FormGroup;
  message: string;
  subject: string;
  recipients: string[];

  recipientsList = [];
  dropDownSettings: IDropdownSettings;
  reasonDropDownSettings: IDropdownSettings;

  reasonsList = [];

  toShowMessage = false;

  reason: string;

  isMessageValid = false;
  isSubjectValid = false;
  // isNumberValid = false;

  contactNumberMask = '(000) 000-0000';

  myMessage: Message;


  // this is the record object
  record;
  // will this to detect recording
  recording = false;
  url: SafeUrl;
  location: LocationMark = null;
  error: string;
  toShowAudioError = false;
  toShowLocationError = false;
  timer = '00:00:00';
  clock = new Date();
  timerInterval = null;

  // ############################## For Map ####################################
  // lat: number = 51.678418;
  // lng: number = 7.809007;

  // markers: Marker[] = [];
  lat: number;
  lng: number;

  constructor(private formBuilder: FormBuilder, private changeDetectorRef: ChangeDetectorRef,
              private usersListService: UsersListService,
              reasonsClass: ReasonsClass,
              private domSanitizer: DomSanitizer,
              private modalService: NgbModal) {
    // this.disableSubject = !OptionsObject.toTakeSubject;
    // this.disableMessage = !OptionsObject.toTakeMessage;

    this.myForm = formBuilder.group({
      message: [{value: null, disabled: !OptionsObject.toTakeMessage}, Validators.required],
      subject: [{value: null, disabled: !OptionsObject.toTakeSubject}, Validators.required],
      number: [null, Validators.required],
      selectedRecipients: [null, Validators.required],
      selectedReasons: [null, Validators.required]
    });

    usersListService.getUsersList().subscribe(usersList => {
      this.recipients = usersList;
    });

    this.recipientsList = [
      {item_id: 1, item_text: this.recipients[0]},
      {item_id: 2, item_text: this.recipients[1]},
      {item_id: 3, item_text: this.recipients[2]},
      {item_id: 4, item_text: this.recipients[3]},
      {item_id: 5, item_text: this.recipients[4]}
    ];

    this.reasonsList = reasonsClass.getReasons();

    this.dropDownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.reasonDropDownSettings = {
      singleSelection: false,
      idField: 'reasonID',
      textField: 'reasonText',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: true
    };

    // this.toDisableMessage = this.options;

    // ###################################### For Map #################################
    // if(navigator){
    //   navigator.geolocation.getCurrentPosition(pos => {
    //     this.lat = +pos.coords.latitude;
    //     this.lng = +pos.coords.longitude;
    //   });
    // }
  }


  get messageValue(): FormControl {
    return this.myForm.get('message') as FormControl;
  }

  get subjectValue(): FormControl {
    return this.myForm.get('subject') as FormControl;
  }

  get numberValue(): FormControl {
    return this.myForm.get('number') as FormControl;
  }

  get selectedRecipientsValue(): FormControl {
    return this.myForm.get('selectedRecipients') as FormControl;
  }

  get selectedReasonsValue(): FormControl {
    return this.myForm.get('selectedReasons') as FormControl;
  }

  ngOnInit(): void {
  }

  showOutput(value: any): void {
    if (!this.url){
      this.toShowAudioError = true;
    }
    else if(!this.location){
      // console.log('hey budddyyyy!!!');
      this.toShowLocationError = true;
    }
    else{
      this.toShowAudioError = false;
      this.toShowLocationError = false;
      // redirect to the output component
      // console.log('location at the time of submission: ');
      // console.log(this.location);
      this.toShowMessage = true;
      this.myMessage = {
        subject: this.subjectValue.value,
        message: this.messageValue.value,
        number: this.numberValue.value,
        audioURL: this.url,
        location: this.location,
        recipients: this.selectedRecipientsValue.value.map(recipient => recipient.item_text),
        reasons: this.selectedReasonsValue.value.map(reason => reason.reasonText)
      };
      // console.log(this.selectedRecipientsValue.value);
      this.myForm.reset();
      this.url = null;
      this.location = null;

      // console.log(this.selectedRecipientsValue.value);
      // this.selectedRecipientsValue.setValue('');
      // console.log(this.selectedRecipientsValue.value);

      AllMessages.push(this.myMessage);
      // console.log(value);
      // console.log('1');
      // console.log(this.selectedRecipientsValue.value);
      // this.selectedRecipientsValue.setValue('');
      // console.log('2');
      // console.log(this.selectedRecipientsValue.value);
    }
  }

  validateMessage(): void {
    this.isMessageValid = this.messageValue.value.trim();
  }

  validateSubject(): void {
    this.isSubjectValid = this.subjectValue.value.trim();
  }


  // validateNumber(): void {
  //   this.isNumberValid = this.numberValue.value.trim();
  // }

  formChanged(): void {
    this.toShowMessage = false;
  }

  // ########################################## Audio Record ########################################
  sanitize(url: string): SafeUrl {
    // console.log('in sanitize');
    // console.log(url);
    // console.log(this.domSanitizer.bypassSecurityTrustUrl(url));
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }

  startTimer(): void{
    const hours = 0;
    const minutes = 0;
    const seconds = 0;
    this.clock.setHours(hours);
    this.clock.setMinutes(minutes);
    this.clock.setSeconds(seconds);
    this.clock.setMilliseconds(0);    // Make sure that milliseconds is 0
    this.timerInterval = setInterval(() => {
      const currentTime = this.clock.getTime();
      this.clock.setTime(currentTime + 1000); // increase a second every time
      // console.log(this.clock);
      // console.log(this.clock.toLocaleTimeString().split(':'));
      // this.timer = this.clock.getHours().toString() + ':' + this.clock.getMinutes().toString() + ':';
      // this.clock.getSeconds().toString();
      const timeStringArray = this.clock.toLocaleTimeString().split(':');
      // console.log(timeStringArray);
      this.timer = timeStringArray[0] + ':' + timeStringArray[1] + ':' + timeStringArray[2];
    }, 1000);
  }

  initiateRecording(): void {
    this.recording = true;

    this.startTimer();

    const mediaConstraints = {
      video: false,
      audio: true
    };

    navigator.mediaDevices.getUserMedia(mediaConstraints).then(this.successCallback.bind(this),
      this.errorCallback.bind(this));
  }

  successCallback(stream): void {
    const options = {
      mimeType: 'audio/wav',
      numberOfAudioChannels: 1
    };

    const StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
    this.record = new StereoAudioRecorder(stream, options);
    this.record.record();
  }

  errorCallback(error): void {
    this.error = 'Can\'t play audio in your browser';
  }

  stopRecording(): void {
    clearInterval(this.timerInterval);
    this.timer = '00:00:00';
    this.recording = false;
    this.record.stop(this.processRecording.bind(this));
  }

  processRecording(blob): void {
    // console.log(blob);
    this.url = this.sanitize(URL.createObjectURL(blob));
  }


  clearAudio(): void {
    this.url = null;
  }

  // ############################### Get Location Modal Content #################################
  getLocation(locationModal: TemplateRef<any>) {
    // console.log('clicked');
    this.toShowLocationError = false;
    this.modalService.open(locationModal, {ariaLabelledBy: 'modal-basic-title'}).result
      .then((result) => {
        // console.log('Result: ' + result);
        // get the location
        if(this.lat){
          this.toShowLocationError = false;
          this.location = {
            lat: this.lat,
            lng: this.lng,
            label: this.subjectValue.value,
            draggable: false
          }
        }
        else{
          this.toShowLocationError = true;
          this.location = null;
        }
      }, (reason) => {
        if(!this.location){
          // console.log('Reason: ' + reason);
          this.toShowLocationError = true;
          this.lat = null;
          this.lng = null;
          this.location = null;
        }
      });
  }

  // markerClicked(label: string, i: number): void {
  //   alert(`clicked the marker: ${label || i}`);
  // }


  mapClicked($event: AgmMouseEvent): void {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
  }

  showCurrentLocation() {
    if(navigator){
      navigator.geolocation.getCurrentPosition(pos => {
        this.lat = +pos.coords.latitude;
        this.lng = +pos.coords.longitude;
      });
    }
  }
}
