import {ChangeDetectorRef, Component, TemplateRef} from '@angular/core';

import {FormBuilder, FormGroup, FormControl, Validators, FormArray} from '@angular/forms';

import {IDropdownSettings} from 'ng-multiselect-dropdown';

import {UsersListService} from "../../../services/users-list.service";

import {OptionsObject} from "../../../data/options";
import {Message} from "../../../classes/Message";
import {AllMessages} from "../../../data/all-messages";
import {ReasonsClass} from "../../../data/reasonsClass";

import * as RecordRTC from 'recordrtc';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';

import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

import {MouseEvent as AgmMouseEvent} from "@agm/core";
import {LocationMark} from "../../../classes/LocationMark";

@Component({
  selector: 'app-forms-array',
  templateUrl: './forms-array.component.html',
  styleUrls: ['./forms-array.component.css']
})
export class FormsArrayComponent{

  myFormArray: FormArray;

  // message: string;
  // subject: string;
  reasonsList = [];
  recipients: string[];
  recipientsList = [];

  allSelectedRecipients: string[];
  currentSelectedRecipient: string;

  dropDownSettings: IDropdownSettings;
  reasonDropDownSettings: IDropdownSettings;

  // reason: string;
  contactNumberMask = '(000) 000-0000';
  myMessage: Message;

  // this is the record object
  record;
  // will this to detect recording
  recording = false;
  // url: SafeUrl;
  // location: LocationMark = null;
  error: string;
  timer = '00:00:00';
  clock = new Date();
  timerInterval = null;

  cantChangeUser = false;

  // ############################## For Map ####################################
  lat: number;
  lng: number;
  toShowSentMessage = false;

  constructor(private formBuilder: FormBuilder, private changeDetectorRef: ChangeDetectorRef,
              private usersListService: UsersListService,
              reasonsClass: ReasonsClass,
              private domSanitizer: DomSanitizer,
              private modalService: NgbModal) {

    this.myFormArray = formBuilder.array([]);

    usersListService.getUsersList().subscribe(usersList => {
      this.recipients = usersList;
    });

    this.recipientsList = [
      {item_id: 1, item_text: this.recipients[0]},
      {item_id: 2, item_text: this.recipients[1]},
      {item_id: 3, item_text: this.recipients[2]},
      {item_id: 4, item_text: this.recipients[3]},
      {item_id: 5, item_text: this.recipients[4]}
    ];

    this.reasonsList = reasonsClass.getReasons();

    this.dropDownSettings = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.reasonDropDownSettings = {
      singleSelection: false,
      idField: 'reasonID',
      textField: 'reasonText',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: true
    };
  }

  addFormGroup($event){
    // console.log('hello');
    // console.log($event);
    // console.log(this.allSelectedRecipients);
    this.toShowSentMessage = false;
    this.lat = null;
    this.lng = null;
    if(this.myFormArray.controls.length < 1){
      this.currentSelectedRecipient = $event.item_text;
      this.toShowSentMessage = false;
    }

    // console.log($event);
    // console.log(this.myFormArray);
    // console.log(this.myFormArray.controls);
    // Check if the recipient's data already exists
    for (let i = 0; i < this.myFormArray.controls.length; i++){
      if(this.myFormArray.controls[i].get('recipient').value === $event.item_text){
        // console.log('found');
        return;
      }
    }

    this.myFormArray.push(this.formBuilder.group({
      message:[{value: null, disabled: !OptionsObject.toTakeMessage}, Validators.required],
      subject: [{value: null, disabled: !OptionsObject.toTakeSubject}, Validators.required],
      number: [null, Validators.required],
      audioURL: [null, Validators.required],
      location: [null, Validators.required],
      recipient: [$event.item_text, Validators.required],
      selectedReasons: [null, Validators.required],
      isMessageValid: [false],
      isSubjectValid: [false],
      toShowAudioError: [false],
      toShowLocationError: [false]
    }));
  }


  changeUser(item_text: any) {
    this.toShowSentMessage = false;
    this.lat = null;
    this.lng = null;
    this.currentSelectedRecipient = item_text;
  }

  sendMessages(): void {
    // console.log('sending...');
    this.toShowSentMessage = true;

    for (let i = 0; i < this.myFormArray.controls.length; i++){
      this.myMessage = {
        subject: this.myFormArray.controls[i].get('subject').value,
        message: this.myFormArray.controls[i].get('message').value,
        number: this.myFormArray.controls[i].get('number').value,
        audioURL: this.myFormArray.controls[i].get('audioURL').value,
        location: this.myFormArray.controls[i].get('location').value,
        recipients: [this.myFormArray.controls[i].get('recipient').value],
        reasons: this.myFormArray.controls[i].get('selectedReasons').value.map(reason => reason.reasonText)
      };

      AllMessages.push(this.myMessage);
    }

    while(this.myFormArray.controls.length > 0){
      // console.log(this.myFormArray.controls.length);
      this.myFormArray.removeAt(0);
    }
    // console.log(this.myFormArray.controls);
    // this.myFormArray.controls[index].reset();
    // this.myFormArray.reset();
    this.currentSelectedRecipient = null;
    this.allSelectedRecipients = null;
    // console.log(this.myFormArray.controls);
  }


  validateMessage(i): void {
    this.myFormArray.controls[i].get('isMessageValid').setValue(this.myFormArray.controls[i].get('message').value.trim());
  }

  validateSubject(i): void {
    this.myFormArray.controls[i].get('isSubjectValid').setValue(this.myFormArray.controls[i].get('subject').value.trim());
  }

  // formChanged(): void {
  //   this.toShowSentMessage = false;
  //   console.log('hes');
  //   // this.myFormArray.controls[i].get('toShowSentMessage').setValue(false);
  // }

  // ########################################## Audio Record ########################################
  sanitize(url: string): SafeUrl {
    // console.log('in sanitize');
    // console.log(url);
    // console.log(this.domSanitizer.bypassSecurityTrustUrl(url));
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }

  startTimer(): void{
    const hours = 0;
    const minutes = 0;
    const seconds = 0;
    this.clock.setHours(hours);
    this.clock.setMinutes(minutes);
    this.clock.setSeconds(seconds);
    this.clock.setMilliseconds(0);    // Make sure that milliseconds is 0
    this.timerInterval = setInterval(() => {
      const currentTime = this.clock.getTime();
      this.clock.setTime(currentTime + 1000); // increase a second every time
      // console.log(this.clock);
      // console.log(this.clock.toLocaleTimeString().split(':'));
      // this.timer = this.clock.getHours().toString() + ':' + this.clock.getMinutes().toString() + ':';
      // this.clock.getSeconds().toString();
      const timeStringArray = this.clock.toLocaleTimeString().split(':');
      // console.log(timeStringArray);
      this.timer = timeStringArray[0] + ':' + timeStringArray[1] + ':' + timeStringArray[2];
    }, 1000);
  }

  initiateRecording(): void {
    this.cantChangeUser = true;
    this.recording = true;

    this.startTimer();

    const mediaConstraints = {
      video: false,
      audio: true
    };

    navigator.mediaDevices.getUserMedia(mediaConstraints).then(this.successCallback.bind(this),
      this.errorCallback.bind(this));
  }

  successCallback(stream): void {
    const options = {
      mimeType: 'audio/wav',
      numberOfAudioChannels: 1
    };

    const StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
    this.record = new StereoAudioRecorder(stream, options);
    this.record.record();
  }

  errorCallback(error): void {
    this.error = 'Can\'t play audio in your browser';
  }

  stopRecording(index): void {
    this.cantChangeUser = false;
    // console.log('stop recording: ' + index);
    clearInterval(this.timerInterval);
    this.timer = '00:00:00';
    this.recording = false;
    this.record.stop(this.processRecording.bind(this, [index]));
  }

  // the arguments were to be reversed, dunno why
  processRecording(indexArray, blob): void {
    // console.log(indexArray);
    // console.log('process recording blob: ' + blob);
    // console.log('process Recording: ' + indexArray[0]);
    // console.log('processing');
    this.myFormArray.controls[indexArray[0]].get('audioURL').setValue(this.sanitize((URL.createObjectURL(blob))));
    // console.log(blob);
    // this.url = this.sanitize(URL.createObjectURL(blob));
  }

  clearAudio(index): void {
    this.myFormArray.controls[index].get('audioURL').setValue(null);
    this.myFormArray.controls[index].get('toShowAudioError').setValue(true);
  }

  // ############################### Get Location Modal Content #################################
  getLocation(index, locationModal: TemplateRef<any>) {
    // console.log('clicked');
    // this.toShowLocationError = false;
    this.myFormArray.controls[index].get('toShowLocationError').setValue(false);
    // console.log('in getLocation()');
    this.modalService.open(locationModal, {ariaLabelledBy: 'modal-basic-title'}).result
      .then((result) => {
        // console.log('Result: ' + result);
        // get the location
        if(this.lat){
          this.myFormArray.controls[index].get('toShowLocationError').setValue(false);
          this.myFormArray.controls[index].get('location').setValue({
            lat: this.lat,
            lng: this.lng,
            label: this.myFormArray.controls[index].get('subject').value,
            draggable: false
          });
        }
        else{
          this.myFormArray.controls[index].get('toShowLocationError').setValue(true);
          this.myFormArray.controls[index].get('location').setValue(null);
        }
      }, (reason) => {
        // console.log('Reason :' + reason);
        if(!this.myFormArray.controls[index].get('location').value){
          // console.log('Reason2 : ' + reason);
          this.myFormArray.controls[index].get('toShowLocationError').setValue(true);
          this.lat = null;
          this.lng = null;
          this.myFormArray.controls[index].get('location').setValue(null);
        }
      });
  }

  mapClicked($event: AgmMouseEvent): void {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;
  }

  showCurrentLocation() {
    if(navigator){
      navigator.geolocation.getCurrentPosition(pos => {
        this.lat = +pos.coords.latitude;
        this.lng = +pos.coords.longitude;
      });
    }
  }

}
