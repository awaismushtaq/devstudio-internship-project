import {Component, OnInit, TemplateRef} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {AdminService} from "../../../services/admin.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormControl} from "@angular/forms";
import {LoadersCSS} from "ngx-loaders-css";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  Orders: any[] = [];
  isDataLoading = true;

  // Orders Table
  columnsToDisplay = ['order-id', 'order-time', 'user-id',
    'order-total', 'order-status', 'action', 'change-status'];
  matTable;

  selectedOrder: any ;

  // For Loader
  // loader: LoadersCSS = 'pacman';
  // bgColor = 'black';
  // color = 'rgba(100, 100, 100, 0.5)';

  statusList: string[] = [
    'Pending',
    'Dispatched',
    'Completed',
    'Cancelled'
  ];

  constructor(private adminService: AdminService,
              private modalService: NgbModal,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    // console.log('in ngOnInit of ProductComponent');
    this.adminService.getAllOrders().then((Orders) => {
      this.Orders = Orders;
      this.matTable = new MatTableDataSource(this.Orders);
      this.isDataLoading = false;
    }).catch((error) => {
      // console.log(error);
      this.Orders = [];
      this.matTable = new MatTableDataSource(this.Orders);
      this.isDataLoading = false;
    });
  }

  showDetails(showDetailsModal: TemplateRef<any>, order: any) {
    this.adminService.getOrderDetails(order).then((snapshot) => {
      this.selectedOrder = snapshot.val();

      this.modalService.open(showDetailsModal, {ariaLabelledBy: 'modal-basic-title'})
        .result.then((result)=>{
          // do nothing
      }).catch((error)=>{
        // console.log(error);
      });
    }).catch((error) => {
      // console.log(error);
    });
  }

  changeOrderStatus(order: any) {
    this.adminService.changeOrderStatus(order).then(()=>{
      // console.log('ha');
      this.toastr.success('Status of Order with ID ' + order.orderID + ' has been changed to ' + order.orderStatus);
    }).catch(()=>{
      // console.log('error');
      this.toastr.error('Error while changing order status');
    })
  }

}
