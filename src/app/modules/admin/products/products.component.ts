// TODO: 'Edit product from table using action column' functionality


import {ChangeDetectorRef, Component, OnInit, TemplateRef} from '@angular/core';
import {Product} from "../../../classes/Product";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AllProductsService} from "../../../services/all-products.service";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  myForm: FormGroup;
  editProductForm: FormGroup;
  Products: Product[];

  // Products Table
  columnsToDisplay: string[] = ['product-id', 'product-image', 'name', 'price', 'description', 'action'];
  matTable;

  // loading bar
  addingProduct = false;
  deletingProduct = false;
  editingProduct = false;

  productBeforeEdit: Product;
  productAfterEdit: Product;

  isDataLoading = true;

  // storing the product that is currently to be deleted
  private productToDelete: Product = null;

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private allProductsService: AllProductsService) {
    this.myForm = formBuilder.group({
      imageURL: [null, Validators.required],
      name: [null, Validators.required],
      description: [null, [Validators.required, Validators.maxLength(100)]],
      price: [null, [Validators.required, Validators.min(0), Validators.max(300)]]
    });

    this.editProductForm = formBuilder.group({
      imageURL: [null, Validators.required],
      name: [null, Validators.required],
      description: [null, [Validators.required, Validators.maxLength(100)]],
      price: [null, [Validators.required, Validators.min(0), Validators.max(300)]]
    });
    // console.log('in ProductComponent constructor');
  }

  ngOnInit(): void {
    // console.log('in ngOnInit of ProductComponent');
    this.Products = [];
    this.matTable = new MatTableDataSource(this.Products);

    this.allProductsService.getInitialData().then((AllProducts) => {
      this.Products = AllProducts;
      this.matTable.data = this.Products;
      this.isDataLoading = false;
    }).catch((error) => {
      // console.log(error);
      this.isDataLoading = false;
    });
  }

  get imageURL(): FormControl {
    return this.myForm.get('imageURL') as FormControl;
  }

  get name(): FormControl {
    return this.myForm.get('name') as FormControl;
  }

  get description(): FormControl {
    return this.myForm.get('description') as FormControl;
  }

  get price(): FormControl {
    return this.myForm.get('price') as FormControl;
  }

  get editImageURL(): FormControl{
    return this.editProductForm.get('imageURL') as FormControl;
  }

  get editName(): FormControl {
    return this.editProductForm.get('name') as FormControl;
  }

  get editDescription(): FormControl {
    return this.editProductForm.get('description') as FormControl;
  }

  get editPrice(): FormControl {
    return this.editProductForm.get('price') as FormControl;
  }

  readURL(imageInput: HTMLInputElement, imageControl: AbstractControl): void {
    if (imageInput.files && imageInput.files[0]) {
      const reader = new FileReader();
      reader.readAsDataURL(imageInput.files[0]);
      reader.onload = (e) => {
        // console.log('before');
        // console.log(this.editProductForm.value);
        imageControl.setValue(reader.result);
        this.editProductForm.markAsDirty();
        // console.log('after');
        // console.log(this.editProductForm.value);

      };
    }
  }

  isKeyPressValid($event: KeyboardEvent): boolean {
    const keyASCII = $event.key.charCodeAt(0);
    return (keyASCII >= 48 && keyASCII <= 57) || keyASCII == 46;
  }

  addProduct(addProductModal: TemplateRef<any>) {
    this.modalService.open(addProductModal, {ariaLabelledBy: 'modal-basic-title'})
      .result.then((result) => {
        // modal.close() is never called
    }).catch((error) => {
      this.addingProduct = false;
      // console.log(error);
    });
  }

  uploadProduct() {
    this.addingProduct = true;
    this.allProductsService.addProduct({
      productID: null,
      imageURL: this.imageURL.value,
      name: this.name.value,
      price: this.price.value,
      description: this.description.value
    }).then(() => {
      this.addingProduct = false;
      this.modalService.dismissAll();
      this.Products = this.allProductsService.getProducts();
      this.matTable.data = this.Products;
      this.myForm.reset();
    }).catch(err => {
      this.addingProduct = false;
    });
  }

  deleteProduct(product: Product, confirmProductDelete: TemplateRef<any>) {
    this.productToDelete = product;
    this.modalService.open(confirmProductDelete, {ariaLabelledBy: 'confirm-product-delete-modal-title'})
      .result.then((result) => {
      // modal.close() is never called
    }).catch((error) => {
      this.deletingProduct = false;
      // console.log(error);
    })
  }

  deleteProductConfirmed() {
    this.deletingProduct = true;
    this.allProductsService.deleteProduct(this.productToDelete).then(() => {
      this.Products = this.allProductsService.getProducts();
      this.matTable.data = this.Products;
      this.modalService.dismissAll();
      this.deletingProduct = false;
      this.productToDelete = null;
    }).catch(()=>{
      this.deletingProduct = false;
      this.productToDelete = null;
    })
  }

  editProduct(product: Product, editProductModal: TemplateRef<any>) {
    // this.editingProduct = true;
    this.editImageURL.setValue(product.imageURL);
    this.editName.setValue(product.name);
    this.editDescription.setValue(product.description);
    this.editPrice.setValue(product.price);
    this.productBeforeEdit = product;

    this.modalService.open(editProductModal, {ariaLabelledBy: 'edit-product-modal-basic-title'})
      .result.then((result)=>{
      // modal.close() is never called
    }).catch((error)=>{
      this.editingProduct = false;
      // console.log(error);
    });
  }

  uploadEditedProduct() {
    this.editingProduct = true;
    this.productAfterEdit = {
      productID: this.productBeforeEdit.productID,
      imageURL: this.editImageURL.value,
      name: this.editName.value,
      price: this.editPrice.value,
      description: this.editDescription.value
    }
    this.allProductsService.editProduct(this.productBeforeEdit, this.productAfterEdit)
      .then(() => {
      this.editingProduct = false;
      this.modalService.dismissAll();
      this.Products = this.allProductsService.getProducts();
      this.matTable.data = this.Products;
      this.editProductForm.reset();
    }).catch(err => {
      this.editingProduct = false;
    });

  }
}
