import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminComponent} from "./admin.component";
import {AdminRoutingModule} from "./admin-routing.module";
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductsComponent } from './products/products.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AllProductsService} from "../../services/all-products.service";
import {MatTableModule} from "@angular/material/table";
import {NgSelectModule} from "@ng-select/ng-select";
import {NgxLoadersCssModule} from "ngx-loaders-css";

@NgModule({
  declarations: [
    AdminComponent,
    DashboardComponent,
    ProductsComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    NgSelectModule,
    NgxLoadersCssModule
  ],
  exports: [
    AdminComponent,
    DashboardComponent,
    ProductsComponent,
    NgxLoadersCssModule
  ],
  providers: [
    AllProductsService
  ]
})
export class AdminModule {
}
