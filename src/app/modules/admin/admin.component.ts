import {Component} from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styles: []
})
export class AdminComponent {
  active = 'products';

  constructor() {
    // console.log('in admin component');
  }
}
