import { Component, OnInit } from '@angular/core';
import {BehaviorSubject, Subject, Observable, ReplaySubject, observable} from 'rxjs';

@Component({
  selector: 'app-behaviorsubjectpractice',
  templateUrl: './behaviorsubjectpractice.component.html',
  styleUrls: ['./behaviorsubjectpractice.component.css']
})
export class BehaviorsubjectpracticeComponent implements OnInit {
  private myObservable: Observable<number>;
  private mySubject = new Subject<number>();
  // The BehaviorSubject class implements a subject that has a current value and,
  // for that reason, an initial value is required.
  // If you need a subject which doesn't have an initial value, use Subject.
  private myBehaviorSubject = new BehaviorSubject<number>(0);
  private myReplaySubject = new ReplaySubject<number>(2);

  constructor() {
    // Create an Observable that will start listening when someone subscribes
    this.myObservable = new Observable<number>(observer => {
      observer.next(Math.random());
    });

    // this.myObservable.next(23);

    this.observableExample();


    // Subject Example
    // console.log('\n\n');
    // console.log('Subject broadcasting 0th value');
    this.mySubject.next(Math.random());
    // console.log('Subject broadcasting first value');
    this.mySubject.next(Math.random());
    this.subjectExample();
    // console.log('Subject broadcasting second value');
    this.mySubject.next(Math.random());

    // BehaviorSubject Example
    // console.log('\n\n');
    // console.log('BehaviorSubject broadcasting 0th value');
    this.myBehaviorSubject.next(Math.random());
    // console.log('BehaviorSubject broadcasting first value');
    this.myBehaviorSubject.next(Math.random());
    this.behaviorSubjectExample();
    // console.log('BehaviorSubject broadcasting second value');
    this.myBehaviorSubject.next(Math.random());

    // // ReplaySubject Example
    // console.log('\n\n');
    // console.log('ReplaySubject broadcasting 0th value');
    this.myReplaySubject.next(Math.random());
    // console.log('ReplaySubject broadcasting first value');
    this.myReplaySubject.next(Math.random());
    this.replaySubjectExample();
    // console.log('ReplaySubject broadcasting second value');
    this.myReplaySubject.next(Math.random());

  }

  ngOnInit(): void {
  }

  observableExample(): void{
    const sub1 = this.myObservable.subscribe({
      next(value){
        // console.log('From sub1 in Observable Example: ' + value);
      },
      error(err){
        // console.log('Error: ' + err);
      }
    });

    const sub2 = this.myObservable.subscribe({
      next(value){
        // console.log('From sub2 in Observable Example: ' + value);
      },
      error(err){
        // console.log('Error: ' + err);
      }
    });
  }

  subjectExample(): void{
    const sub1 = this.mySubject.subscribe({
      next(value){
        // console.log('From sub1 in Subject Example: ' + value);
      },
      error(err){
        // console.log('Error: ' + err);
      }
    });

    const sub2 = this.mySubject.subscribe({
      next(value){
        // console.log('From sub2 in Subject Example: ' + value);
      },
      error(err){
        // console.log('Error: ' + err);
      }
    });
  }

  behaviorSubjectExample(): void{
    const sub1 = this.myBehaviorSubject.subscribe({
      next(value){
        // console.log('From sub1 in BehaviorSubject Example: ' + value);
      },
      error(err){
        // console.log('Error: ' + err);
      }
    });

    const sub2 = this.myBehaviorSubject.subscribe({
      next(value){
        // console.log('From sub2 in BehaviorSubject Example: ' + value);
      },
      error(err){
        // console.log('Error: ' + err);
      }
    });
  }

  replaySubjectExample(): void{
    const sub1 = this.myReplaySubject.subscribe({
      next(value){
        // console.log('From sub1 in ReplaySubject Example: ' + value);
      },
      error(err){
        // console.log('Error: ' + err);
      }
    });

    const sub2 = this.myReplaySubject.subscribe({
      next(value){
        // console.log('From sub2 in ReplaySubject Example: ' + value);
      },
      error(err){
        // console.log('Error: ' + err);
      }
    });
  }

}
