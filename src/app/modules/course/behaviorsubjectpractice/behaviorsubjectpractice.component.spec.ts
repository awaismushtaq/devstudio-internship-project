import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BehaviorsubjectpracticeComponent } from './behaviorsubjectpractice.component';

describe('BehaviorsubjectpracticeComponent', () => {
  let component: BehaviorsubjectpracticeComponent;
  let fixture: ComponentFixture<BehaviorsubjectpracticeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BehaviorsubjectpracticeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BehaviorsubjectpracticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
