import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard/dashboard.component';
import { CourseListComponent } from './course-list/course-list.component';
import { CourseDetailsComponent } from './course-details/course-details.component';
import {CourseRoutingModule} from './course-routing.module';
import {CourseComponent} from './course.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AdditionPipe} from "../../pipes/addition.pipe";
import {ExponentPipe} from "../../pipes/exponent.pipe";

import {BehaviorsubjectpracticeComponent} from './behaviorsubjectpractice/behaviorsubjectpractice.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [
    DashboardComponent,
    CourseListComponent,
    CourseDetailsComponent,
    CourseComponent,
    AdditionPipe,
    ExponentPipe,
    BehaviorsubjectpracticeComponent
  ],
  imports: [
    CommonModule,
    CourseRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ],
  exports: [
    DashboardComponent,
    CourseDetailsComponent,
    CourseListComponent,
    CourseComponent
  ]
})
export class CourseModule { }
