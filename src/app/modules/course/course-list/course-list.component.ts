import { Component, OnInit } from '@angular/core';

import {Course} from "../../../classes/Course";
import {CourseService} from "../../../services/course.service";

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  courses: Course[];
  selectedCourse: Course;
  receivedString: string;
  myDate = new Date();

  // To add a private CoursesService parameter of the CourseService to the constructor, we use
  // 'private <service name>: <type of service>'
  constructor(private coursesService: CourseService) { }

  ngOnInit(): void {
    this.getCourses();
  }

  onSelect(course: Course): void{
    this.selectedCourse = course;
  }

  getCourses(): void{
    this.coursesService.getCourses().subscribe(courses => this.courses = courses);
  }

  receiveEmittedEvent($event: any): void{
    // console.log('1: ' + $event);
    this.receivedString = $event;
  }
}
