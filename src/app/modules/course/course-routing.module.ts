import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CourseListComponent} from './course-list/course-list.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CourseComponent} from './course.component';
import {CourseDetailsComponent} from './course-details/course-details.component';

// const routes: Routes = [
//   {
//     path: '',
//     component: CourseComponent
//   },
//   {
//     path: 'course-list',
//     component: CourseListComponent
//   },
//   {
//     path: 'dashboard/details/:id',
//     component: CourseDetailsComponent
//   },
//   {
//     path: 'dashboard',
//     component: DashboardComponent
//   }
// ];

const routes: Routes = [
  {
    path: '',
    component: CourseComponent
  },
  {
    path: 'details/:id',
    component: CourseDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseRoutingModule {
}
