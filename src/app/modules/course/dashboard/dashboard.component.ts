import { Component, OnInit } from '@angular/core';
import {Course} from "../../../classes/Course";
import {CourseService} from "../../../services/course.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

  courses: Course[];
  selectedCourse: Course;

  constructor(private coursesService: CourseService) { }

  ngOnInit(): void {
    this.getCourses();
  }

  // The return type is void
  // onSelect(course: Course): void{
  //   this.selectedCourse = course;
  // }

  getCourses(): void{
    this.coursesService.getCourses().subscribe(courses => this.courses = courses);
  }
}







