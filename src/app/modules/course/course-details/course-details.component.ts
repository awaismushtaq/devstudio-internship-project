import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import {Course} from "../../../classes/Course";
import {CourseService} from "../../../services/course.service";


@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.css']
})

export class CourseDetailsComponent implements OnInit {

  // id: number;
  courses: Course[];

  @Input() course: Course;
  @Output() courseNameClick = new EventEmitter();

  constructor(private route: ActivatedRoute, private coursesService: CourseService) {}

  ngOnInit(): void {
    // this.route gives us a snapshot of the current route
    // from this.route.snapshot we use the paramMap API which helps us get the parameter from the URL
    // and for us the parameter is the id
    const id = parseInt(this.route.snapshot.paramMap.get('id'));

    if (id){
      // this.courses = COURSES;

      this.coursesService.getCourses().subscribe(courses => this.courses = courses);

      // console.log(this.courses);

      this.course = this.courses.find(element =>{
        return element.id === id;
      });

      // console.log(this.course);
    }
  }

  emitName(): void {
    this.courseNameClick.emit('Name:');
  }
}
