import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from "../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class AdminPageGuard implements CanActivate {

  constructor(private authService: AuthService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {


    // console.log('in AdminPageGuard');
    // console.log(this.authService.isAdminLoggedIn.getValue());
    // return this.authService.isAdminLoggedIn.getValue();

    return JSON.parse(localStorage.getItem('currentUser')).userStatus === 'admin';
  }

}
