import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from "../services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class StoreGuard implements CanActivate {

  constructor(private authService: AuthService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    // console.log('in StoreGuard');
    // console.log(this.authService.isUserLoggedIn.getValue());
    // return this.authService.isUserLoggedIn.getValue();

    return JSON.parse(localStorage.getItem('currentUser')).userStatus === 'normal-user';
  }

}
