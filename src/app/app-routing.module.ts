import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {CourseModule} from "./modules/course/course.module";
import {ShopModule} from "./modules/shop/shop.module";
import {MapComponent} from "./components/map/map.component";
import {AuthPageGuard} from "./guards/auth-page.guard";
import {AdminPageGuard} from "./guards/admin-page.guard";
import {StoreGuard} from "./guards/store.guard";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'courses',
    pathMatch: 'full',
  },
  {
    path: 'map',
    component: MapComponent
  },
  {
    path: 'courses',
    loadChildren: () => CourseModule
  },
  {
    path: 'shop',
    loadChildren: () => ShopModule
  },
  {
    path: 'api-requests',
    loadChildren: () => import('./modules/api-requests/api-requests.module').then(m => m.ApiRequestsModule)
  },
  {
    path: 'mail',
    loadChildren: () => import('./modules/mail/mail.module').then(m => m.MailModule)
  },
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
    canActivate: [AuthPageGuard]
  },
  {
    path: 'admin',
    loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule),
    canActivate: [AdminPageGuard]
  },
  {
    path: 'store',
    loadChildren: () => import('./modules/store/store.module').then(m => m.StoreModule),
    canActivate: [StoreGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
