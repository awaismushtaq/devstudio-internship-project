import {Component} from '@angular/core';
import {AuthService} from "./services/auth.service";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-project';
  active: number = 0;

  isUserLoggedIn = false;
  isAdminLoggedIn = false;

  constructor(private authService: AuthService,
              private toastr: ToastrService,
              private router: Router) {
    authService.isAdminLoggedIn.subscribe((res) => {
      this.isAdminLoggedIn = res;
    });

    authService.isUserLoggedIn.subscribe((res) => {
      this.isUserLoggedIn = res;
    });
  }

  signout() {
    this.authService.SignOut().then((result) => {
      this.toastr.success('You have successfully signed out!', 'Signout Successful');
      this.router.navigate(['auth']);
    }).catch((error) => {
      this.toastr.error(error, 'Signout Error');
    })
  }
}
