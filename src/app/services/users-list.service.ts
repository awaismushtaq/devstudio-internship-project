import {Injectable} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})


export class UsersListService {

  USERSLIST: string[] = [
    'user1',
    'user2',
    'user3',
    'user4',
    'user5'
  ];

  constructor() {
  }

  getUsersList(): Observable<string[]> {
    return of (this.USERSLIST);
  }

}
