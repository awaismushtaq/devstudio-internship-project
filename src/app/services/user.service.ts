import {Injectable} from '@angular/core';
import {AngularFireDatabase} from "@angular/fire/database";
import DataSnapshot = firebase.database.DataSnapshot;
import * as firebase from "firebase";
import {AngularFireStorage} from "@angular/fire/storage";
import {AngularFireAuth} from "@angular/fire/auth";
import {FirebaseApp} from "@angular/fire";
import {AuthService} from "./auth.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private afDatabase: AngularFireDatabase,
              private afStorage: AngularFireStorage,
              private firebaseApp: FirebaseApp,
              private authService: AuthService) {
  }

  getOrderDetails(order: any): Promise<DataSnapshot> {
    const currentUserID = JSON.parse(localStorage.getItem('currentUser')).user.uid;
    return this.afDatabase.database.ref('orders/' + currentUserID + '/' + order.orderID + '/').once('value');
  }

  getOrderHistory(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      const currentUserID = JSON.parse(localStorage.getItem('currentUser')).user.uid;

      this.afDatabase.database.ref('orders/' + currentUserID).once('value')
        .then((snapshot) => {
          let ordersToReturn = [];

          if (snapshot.val()) {
            let orderIDs = Object.keys(snapshot.val());
            for (let j = 0; j < orderIDs.length; j++) {
              if (orderIDs[j] !== null && orderIDs[j] !== 'null') {
                // orderTime: new Date(parseInt(orderIDs[j])).toISOString().substring(0, 10),
                ordersToReturn.push({
                  orderID: orderIDs[j],
                  orderTime: new Date(parseInt(orderIDs[j])).toISOString(),
                  orderTotal: snapshot.val()[orderIDs[j]].totalPrice,
                  orderStatus: snapshot.val()[orderIDs[j]].orderStatus
                });
              }
            }
          }
          resolve(ordersToReturn);
        }).catch((error) => {
        reject(null);
      });
    });
  }

  getUserData(): Promise<any> {
    return new Promise((resolve, reject) => {
      const currentUser = JSON.parse(localStorage.getItem('currentUser')).user;
      if (currentUser !== null && currentUser !== 'null') {
        const currentUserID = currentUser.uid;
        this.afDatabase.database.ref('users/' + currentUserID).once('value')
          .then((snapshot) => {
            resolve({
              Email: snapshot.val().Email,
              FName: snapshot.val().FName,
              LName: snapshot.val().LName,
              Title: snapshot.val().Title,
              Address: snapshot.val().Address,
              imageURL: snapshot.val().imageURL
            });
          }).catch((error) => {
          reject(null);
        })
      }
    });
  }


  // getUserData(): Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     const currentUser = JSON.parse(localStorage.getItem('currentUser'));
  //     if (currentUser !== null && currentUser !== 'null') {
  //       const currentUserID = currentUser.user.uid;
  //       const databaseLink = currentUser.userStatus === 'admin' ? 'admin/' : 'users/';
  //       this.firebaseApp.database().ref(databaseLink + currentUserID).once('value')
  //         .then((snapshot) => {
  //           resolve({
  //             Email: snapshot.val().Email,
  //             FName: snapshot.val().FName,
  //             LName: snapshot.val().LName,
  //             Title: snapshot.val().Title,
  //             Address: snapshot.val().Address
  //           });
  //         }).catch((error) => {
  //         reject(null);
  //       })
  //     }
  //   });
  // }


  updateProfileData(previousData: any, updatedData: any): Promise<any> {
    return new Promise<void>((resolve, reject) => {
      if (previousData.imageURL !== updatedData.imageURL) {
        // this means that image was changed
        // console.log('in if');
        // console.log(previousData);
        this.afStorage.storage.refFromURL(previousData.imageURL).delete()
          .then((result) => {
            const uniqueFileName = Date.now();
            // the imageURL contains data like this 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAA.'
            const imageBase64 = updatedData.imageURL.split(',')[1];
            const imageType = updatedData.imageURL.split(';')[0].split('/')[1];
            const storageRef = this.afStorage.storage.ref('profile-pictures/' + uniqueFileName + '.' + imageType);
            const task = storageRef.putString(imageBase64, 'base64');

            task.on(firebase.storage.TaskEvent.STATE_CHANGED,
              (snapshot) => {
                // in progress
                // uploadProgress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              },
              (err) => {
                reject()
                // upload failed
                // console.log(err);
              },
              () => {
                // upload success
                storageRef.getDownloadURL().then((downloadURL) => {
                  const currentUserID = JSON.parse(localStorage.getItem('currentUser')).user.uid;
                  this.afDatabase.database.ref('users/' + currentUserID).set({
                    Title: previousData.Title,
                    FName: updatedData.FName,
                    LName: updatedData.LName,
                    Email: updatedData.Email,
                    Address: updatedData.Address,
                    imageURL: downloadURL
                  })
                    .then((result) => {
                      resolve();
                    }).catch((error) => {
                    reject();
                  }).catch((error) => {
                    reject();
                  });
                });
              });
          }).catch((error) => {
          reject();
        })
      } else {
        // no need to change the image
        const currentUserID = JSON.parse(localStorage.getItem('currentUser')).user.uid;
        // console.log(currentUserID);
        // console.log(previousData);
        this.afDatabase.database.ref('users/' + currentUserID).set({
          Title: previousData.Title,
          FName: updatedData.FName,
          LName: updatedData.LName,
          Email: updatedData.Email,
          Address: updatedData.Address,
          imageURL: previousData.imageURL
        }).then((result) => {
            resolve();
        }).catch((error) => {
          reject();
        }).catch((error) => {
          reject();
        });
      }
    });
  }

  updatePassword(currentPassword: string, newPassword: string): Promise<string> {
    // console.log(currentPassword);
    // console.log(newPassword);
    return new Promise<string>((resolve, reject)=>{
      const currentUser = this.firebaseApp.auth().currentUser;
      // update password requires recent authentication
      this.authService.LogIn({Email: currentUser.email, Password: currentPassword})
        .then((result)=>{
          currentUser.updatePassword(newPassword).then(()=>{
            // update successful
            resolve('successful');
          }).catch((err) => {
            // console.log(err);
            reject(err.message);
          });

        }).catch((error)=>{
          reject('Wrong current password!');
      })



      // currentUser.updatePassword(password).then(()=>{
      //   // update successful
      //   resolve();
      // }).catch(()=>{
      //   reject();
      // });
    });
  }
}
