import {Injectable} from '@angular/core';
import {Product} from "../classes/Product";
import {AngularFireStorage} from "@angular/fire/storage";
import * as firebase from 'firebase';
import {AngularFireDatabase} from "@angular/fire/database";

@Injectable({
  providedIn: 'root'
})
export class AllProductsService {
  private AllProducts: Product[];

  constructor(private afStorage: AngularFireStorage, private afDatabase: AngularFireDatabase) {
    // console.log('in AllProductsService constructor');
  }

  getInitialData(): Promise<Product[]> {
    return new Promise((resolve, reject) => {
      this.fetchProductsFromFirebase()
        .then((result) => {

          // fetch from the database
          this.AllProducts = [];
          // console.log(result.val());

          // 0th index of fetched data is null
          if (result.val()) {
            // console.log('result.val()');
            // console.log(result.val());
            for (let i = 0; i < result.val().length; i++) {
              // console.log(result.val()[i]);
              if (result.val()[i]) {
                // console.log('in');
                this.AllProducts.push({
                  name: result.val()[i].productName,
                  price: result.val()[i].productPrice,
                  productID: result.val()[i].productID,
                  description: result.val()[i].productDescription,
                  imageURL: result.val()[i].productImageURL
                })
              }
            }
          }
          // console.log(this.AllProducts);
          resolve(this.AllProducts);
        }).catch((error) => {
        // console.log('error');
        // console.log(error);
        reject(null);
      })
    })
  }

  getProducts(): Product[] {
    return this.AllProducts;
  }

  addProduct(product: Product): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      let newProductID: number;
      if (this.AllProducts && (this.AllProducts.length > 0)) {
        newProductID = Math.max.apply(Math, this.AllProducts.map((p) => p.productID)) + 1;
      } else {
        newProductID = 1;
      }
      product.productID = newProductID;

      // Add Product to Firebase
      const uniqueFileName = Date.now();
      // the imageURL contains data like this 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAA.'
      const imageBase64 = product.imageURL.split(',')[1];
      const imageType = product.imageURL.split(';')[0].split('/')[1];
      const storageRef = this.afStorage.storage.ref('product-images/' + uniqueFileName + '.' + imageType);
      let uploadProgress = 0;
      const task = storageRef.putString(imageBase64, 'base64');
      task.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot) => {
          // in progress
          uploadProgress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        },
        (err) => {
          // upload failed
          // console.log(err);
        },
        () => {
          // upload success
          storageRef.getDownloadURL().then((downloadURL) => {
            this.afDatabase.database.ref('products/' + product.productID).set({
              productID: product.productID,
              productName: product.name,
              productPrice: product.price,
              productDescription: product.description,
              productImageURL: downloadURL
            }).then((result) => {
              product.imageURL = downloadURL;
              this.AllProducts.push(product);
              resolve();
            }).catch((error) => {
              // console.log(error);
              reject();
            });
          })
        });
    });
  }

  editProduct(previousProduct: Product, editedProduct: Product): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      if (previousProduct.imageURL !== editedProduct.imageURL) {
        // this means that image was changed
        this.afStorage.storage.refFromURL(previousProduct.imageURL).delete()
          .then((result) => {
            const uniqueFileName = Date.now();
            // the imageURL contains data like this 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAA.'
            const imageBase64 = editedProduct.imageURL.split(',')[1];
            const imageType = editedProduct.imageURL.split(';')[0].split('/')[1];
            const storageRef = this.afStorage.storage.ref('product-images/' + uniqueFileName + '.' + imageType);
            const task = storageRef.putString(imageBase64, 'base64');

            task.on(firebase.storage.TaskEvent.STATE_CHANGED,
              (snapshot) => {
                // in progress
                // uploadProgress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              },
              (err) => {
                reject()
                // upload failed
                // console.log(err);
              },
              () => {
                // upload success
                storageRef.getDownloadURL().then((downloadURL) => {
                  this.afDatabase.database.ref('products/' + previousProduct.productID).set({
                    productID: editedProduct.productID,
                    productName: editedProduct.name,
                    productPrice: editedProduct.price,
                    productDescription: editedProduct.description,
                    productImageURL: downloadURL
                  })
                    .then((result) => {
                      editedProduct.imageURL = downloadURL;
                      // update product locally

                      const ind = this.AllProducts.findIndex(prod => prod.productID === editedProduct.productID);
                      this.AllProducts[ind].imageURL = editedProduct.imageURL;
                      this.AllProducts[ind].name = editedProduct.name;
                      this.AllProducts[ind].price = editedProduct.price;
                      this.AllProducts[ind].description = editedProduct.description;

                      // this.AllProducts.push(editedProduct);
                      resolve();
                    }).catch((error) => {
                    // console.log(error);
                    reject();
                  }).catch((error) => {
                    // console.log(error);
                    reject();
                  });
                });
              });
          }).catch((error) => {
          reject();
        })
      } else {
        // no need to change the image
        this.afDatabase.database.ref('products/' + previousProduct.productID).set({
          productID: editedProduct.productID,
          productName: editedProduct.name,
          productPrice: editedProduct.price,
          productDescription: editedProduct.description,
          productImageURL: previousProduct.imageURL
        })
          .then((result) => {
            const ind = this.AllProducts.findIndex(prod => prod.productID === editedProduct.productID);
            this.AllProducts[ind].imageURL = editedProduct.imageURL;
            this.AllProducts[ind].name = editedProduct.name;
            this.AllProducts[ind].price = editedProduct.price;
            this.AllProducts[ind].description = editedProduct.description;

            // this.AllProducts.push(editedProduct);
            resolve();
          }).catch((error) => {
          // console.log(error);
          reject();
        }).catch((error) => {
          // console.log(error);
          reject();
        });
      }
    });
  }

  deleteProduct(product: Product): Promise<void> {
    this.AllProducts = this.AllProducts.filter((p) => p.productID !== product.productID);
    return new Promise<void>((resolve, reject) => {
      this.afDatabase.database.ref('products/' + product.productID).remove().then(() => {
        this.afStorage.storage.refFromURL(product.imageURL).delete().then(() => {
          resolve();
        }).catch(() => {
          reject();
        })
      }).catch(() => {
        reject();
      })
    });
  }

  fetchProductsFromFirebase(): Promise<any> {
    // TODO: check if admin is signed in
    return this.afDatabase.database.ref('products/').once('value');
  }

}
