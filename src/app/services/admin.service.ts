import {Injectable} from '@angular/core';
import {AngularFireDatabase} from "@angular/fire/database";
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private AllOrders: any[];

  constructor(private afDatabase: AngularFireDatabase) {
  }

  getAllOrders(): Promise<any> {
    // TODO: check if admin is signed in
    return new Promise<any>((resolve, reject) => {
      this.afDatabase.database.ref('orders/').once('value')
        .then((snapshot) => {
          this.AllOrders = [];

          if (snapshot.val()) {
            const userIDs = Object.keys(snapshot.val());
            for (let i = 0; i < userIDs.length; i++) {
              if (userIDs[i] !== null && userIDs[i] !== 'null') {
                let orderIDs = Object.keys(snapshot.val()[userIDs[i]]);
                for (let j = 0; j < orderIDs.length; j++) {
                  if (orderIDs[j] !== null && orderIDs[j] !== 'null') {
                    // orderTime: new Date(parseInt(orderIDs[j])).toISOString().substring(0, 10),
                    this.AllOrders.push({
                      orderID: orderIDs[j],
                      userID: userIDs[i],
                      orderTime: new Date(parseInt(orderIDs[j])).toISOString(),
                      orderTotal: snapshot.val()[userIDs[i]][orderIDs[j]].totalPrice,
                      orderStatus: snapshot.val()[userIDs[i]][orderIDs[j]].orderStatus
                    });
                  }
                }
              }
            }
          }
          resolve(this.AllOrders);
        }).catch((error) => {
        reject(null);
      })

    })
  }

  // getOrderDetails(order: any): Promise<DataSnapshot> {
  //   if(JSON.parse(localStorage.getItem('currentUser')).userStatus === 'admin'){
  //     return this.afDatabase.database.ref('orders/' + order.userID + '/' + order.orderID + '/').once('value');
  //   }
  //   else{
  //     // order does not contain user id
  //     const currentUserID = JSON.parse(localStorage.getItem('currentUser')).user.uid;
  //     return this.afDatabase.database.ref('orders/' + currentUserID + '/' + order.orderID + '/').once('value');
  //   }
  // }
  getOrderDetails(order: any): Promise<DataSnapshot> {
    return this.afDatabase.database.ref('orders/' + order.userID + '/' + order.orderID + '/').once('value');
  }

  changeOrderStatus(order: any): Promise<any> {
    return this.afDatabase.database.ref('orders/' + order.userID + '/' + order.orderID + '/' +
      'orderStatus').set(order.orderStatus);
  }
}

