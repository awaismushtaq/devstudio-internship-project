// import { Injectable } from "@angular/core";
// import { Comments } from "../classes/comments";
// import {Observable} from "rxjs";
// import { HttpClient, HttpParams } from "@angular/common/http";
// import { Posts } from "../classes/posts";
//
// @Injectable()
// export class freeAPIService{
//
//   constructor(private httpClient: HttpClient){
//
//   }
//
//   getComments(): Observable<any>{
//     return this.httpClient.get('http://jsonplaceholder.typicode.com/posts/1/comments');
//   }
//
//   getCommentsByParameter(): Observable<any>{
//     let param1 = new HttpParams().set('userId', '1');
//     return this.httpClient.get('http://jsonplaceholder.typicode.com/posts', {
//       params: param1
//     });
//   }
//
//   post(myPost: Posts): Observable<any>{
//     return this.httpClient.post('http://jsonplaceholder.typicode.com/posts', myPost);
//   }
//
//   put(myPost: Posts): Observable<any>{
//     // put request for object of id = 1
//     return this.httpClient.put('http://jsonplaceholder.typicode.com/posts/1', myPost);
//   }
//
//   patch(myPost: Posts): Observable<any>{
//     return this.httpClient.patch('http://jsonplaceholder.typicode.com/posts/1', myPost);
//   }
//
//   // Delete returns a message
//   delete(): Observable<any>{
//     return this.httpClient.delete('http://jsonplaceholder.typicode.com/posts/1');
//   }
//
//   //######################################## For Albums ################################
//   getAlbums(): Observable<any>{
//     return this.httpClient.get('http://jsonplaceholder.typicode.com/albums');
//   }
//
//   getPhotosForSelectedAlbumByParameter(selectedAlbumID: string): Observable<any>{
//     let param1 = new HttpParams().set('albumId', selectedAlbumID);
//     return this.httpClient.get('http://jsonplaceholder.typicode.com/photos', {
//       params: param1
//     });
//   }
// }
//
//
// import { Injectable } from "@angular/core";
// import { Comments } from "../classes/comments";
// import {Observable} from "rxjs";
// import { HttpClient, HttpParams } from "@angular/common/http";
// import { Posts } from "../classes/posts";
//
// @Injectable()
// export class freeAPIService{
//
//   constructor(private httpClient: HttpClient){
//
//   }
//
//   getComments(): Observable<any>{
//     return this.httpClient.get('http://jsonplaceholder.typicode.com/posts/1/comments');
//   }
//
//   getCommentsByParameter(): Observable<any>{
//     let param1 = new HttpParams().set('userId', '1');
//     return this.httpClient.get('http://jsonplaceholder.typicode.com/posts', {
//       params: param1
//     });
//   }
//
//   post(myPost: Posts): Observable<any>{
//     return this.httpClient.post('http://jsonplaceholder.typicode.com/posts', myPost);
//   }
//
//   put(myPost: Posts): Observable<any>{
//     // put request for object of id = 1
//     return this.httpClient.put('http://jsonplaceholder.typicode.com/posts/1', myPost);
//   }
//
//   patch(myPost: Posts): Observable<any>{
//     return this.httpClient.patch('http://jsonplaceholder.typicode.com/posts/1', myPost);
//   }
//
//   // Delete returns a message
//   delete(): Observable<any>{
//     return this.httpClient.delete('http://jsonplaceholder.typicode.com/posts/1');
//   }
//
//   //######################################## For Albums ################################
//   getAlbums(): Observable<any>{
//     return this.httpClient.get('http://jsonplaceholder.typicode.com/albums');
//   }
//
//   getPhotosForSelectedAlbumByParameter(selectedAlbumID: string): Observable<any>{
//     let param1 = new HttpParams().set('albumId', selectedAlbumID);
//     return this.httpClient.get('http://jsonplaceholder.typicode.com/photos', {
//       params: param1
//     });
//   }
// }
//
