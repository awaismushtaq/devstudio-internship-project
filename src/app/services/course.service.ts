import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Course} from '../classes/Course';
import {COURSES} from '../data/mock-courses';

// Make the CourseService available to the dependency injection system before angular can inject it.
// For that, I'll have to register a provider. A provider is something that can create or deliver a service.
// In our case, it is going to instantiate the CourseService class to provide the service.
// As you can see below(in @Injectable), angular(by default) register the provider with the root injector for your
// service by including provider metadata
// When you provide the service at the root level, angular creates a single shared instance for the core
// service and injects it into any class that asks for it.

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  constructor() { }

  getCourses(): Observable<Course[]>{
    return of (COURSES);
  }
}
