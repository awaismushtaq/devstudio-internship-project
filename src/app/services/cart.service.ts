import {Injectable} from '@angular/core';
import {BehaviorSubject, Subject} from "rxjs";
import {Product} from "../classes/Product";
import {FirebaseApp} from "@angular/fire";
import {AngularFireDatabase} from "@angular/fire/database";

@Injectable({
  providedIn: 'root'
})
export class CartService {

  noOfItems = new BehaviorSubject<number>(0);
  cartProducts: { product: Product, quantity: number }[] = [];
  cartTotal = new BehaviorSubject<number>(0);
  cartSubtotal = new BehaviorSubject<number>(0);
  cartTax = new BehaviorSubject<number>(0.10);

  constructor(private firebaseApp: FirebaseApp,
              private afDatabase: AngularFireDatabase) {
    this.noOfItems.next(0);
    this.cartProducts = JSON.parse(localStorage.getItem('cartProducts'));
    if (!this.cartProducts) {
      this.cartProducts = [];
      localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
    }
    this.noOfItems.next(this.cartProducts.length)
    this.cartTotal.next(this.getTotal());
    this.cartSubtotal.next(this.getSubtotal());
  }

  addProduct(product: Product): void {
    // find if the product exists in the cart, if so, increment it
    for (let i = 0; i < this.cartProducts.length; i++) {
      if (this.cartProducts[i].product.productID === product.productID) {
        this.cartProducts[i].quantity += 1;
        localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
        this.cartTotal.next(this.getTotal());
        this.cartSubtotal.next(this.getSubtotal());
        return;
      }
    }
    this.cartProducts.push({
      product: product,
      quantity: 1
    });
    this.noOfItems.next(this.noOfItems.getValue() + 1);
    localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
    this.cartTotal.next(this.getTotal());
    this.cartSubtotal.next(this.getSubtotal());
    return;
  }

  removeProduct(product: Product): void {
    this.cartProducts = this.cartProducts.filter(p => p.product.productID !== product.productID)
    this.noOfItems.next(this.noOfItems.getValue() - 1);
    localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
    this.cartTotal.next(this.getTotal());
    this.cartSubtotal.next(this.getSubtotal());
  }

  getProducts(): any[] {
    return this.cartProducts;
  }

  incrementQuantity(product: Product): boolean {
    const ind = this.cartProducts.findIndex(p => p.product === product);
    if (this.cartProducts[ind].quantity < 30) {
      // max 30
      this.cartProducts[ind].quantity += 1;
      localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
      this.cartTotal.next(this.getTotal());
      this.cartSubtotal.next(this.getSubtotal());
      return true;
    }
    return false;
  }

  decrementQuantity(product: Product): boolean {
    const ind = this.cartProducts.findIndex(p => p.product === product);
    if (this.cartProducts[ind].quantity > 1) {
      // minimum 1 quantity
      this.cartProducts[ind].quantity -= 1;
      localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
      this.cartTotal.next(this.getTotal());
      this.cartSubtotal.next(this.getSubtotal());
      return true;
    }
    return false;
  }

  private getTotal(): number {
    return this.getSubtotal() + (this.cartTax.getValue() * this.getSubtotal());
  }

  private getSubtotal(): number {
    let subtotal = 0;

    for (let i = 0; i < this.cartProducts.length; i++) {
      subtotal += this.cartProducts[i].product.price * this.cartProducts[i].quantity;
    }
    return subtotal;
  }

  checkout(): Promise<string> {
    const userID = JSON.parse(localStorage.getItem('currentUser')).user.uid;
    const uniqueOrderID = Date.now();
    return new Promise<string>((resolve, reject) => {
      this.afDatabase.database.ref('orders/' + userID + '/' + uniqueOrderID).set({
        totalPrice: this.getTotal(),
        orderStatus: 'Pending',
        orderedProducts: this.cartProducts
      }).then((result) => {
        this.cartProducts = [];

        this.noOfItems.next(0);
        this.cartProducts = [];
        localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));
        this.cartTotal.next(0);
        this.cartSubtotal.next(0);
        resolve('Order Placed Successfully');
      }).catch((error) => {
        reject(error.message);
      })
    })

  }
}
