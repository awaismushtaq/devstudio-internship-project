import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ChangeActiveService {

  activeTab = new Subject();

  constructor() { }
}
