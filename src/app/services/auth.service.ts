import {Injectable} from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import {FirebaseApp} from "@angular/fire";
import {BehaviorSubject, Subject} from "rxjs";
import * as firebase from "firebase";
import {AngularFireStorage} from "@angular/fire/storage";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // userData: any;
  isUserLoggedIn = new BehaviorSubject<boolean>(false);

  isAdminLoggedIn = new BehaviorSubject<boolean>(false);

  constructor(private afAuth: AngularFireAuth, private firebaseApp: FirebaseApp,
              private afStorage: AngularFireStorage) {
    // console.log('in AuthService constructor');

    const user = JSON.parse(localStorage.getItem('currentUser'));

    if (user) {
      if (user.userStatus === 'admin') {
        this.isAdminLoggedIn.next(true);
        this.isUserLoggedIn.next(false);
      } else {
        this.isAdminLoggedIn.next(false);
        this.isUserLoggedIn.next(true);
      }
    }

  }

  publishLoginStatus(): void {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser !== 'null' && currentUser !== null) {
      if (currentUser.userStatus === 'admin') {
        this.isAdminLoggedIn.next(true);
      } else {
        this.isUserLoggedIn.next(true);
      }
    } else {
      this.isAdminLoggedIn.next(false);
      this.isUserLoggedIn.next(false);
    }
  }

  SignUp(formValue: any) {
    return new Promise((resolve, reject) => {
      this.firebaseApp.auth().createUserWithEmailAndPassword(formValue.Email, formValue.Password)
        .then((result) => {
          const userID = result.user.uid;
          this.isUserLoggedIn.next(true);
          localStorage.setItem('currentUser', JSON.stringify({
            userStatus: 'normal-user',
            user: result.user
          }));

          const uniqueFileName = Date.now();
          // the imageURL contains data like this 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAA.'
          const imageBase64 = formValue.imageURL.split(',')[1];
          const imageType = formValue.imageURL.split(';')[0].split('/')[1];
          const storageRef = this.afStorage.storage.ref('profile-pictures/' + uniqueFileName + '.' + imageType);
          // let uploadProgress = 0;
          const task = storageRef.putString(imageBase64, 'base64');
          task.on(firebase.storage.TaskEvent.STATE_CHANGED, (snapshot) => {
              // in progress
              // uploadProgress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            },
            (err) => {
              const currentUser = this.firebaseApp.auth().currentUser;
              this.isUserLoggedIn.next(false);
              localStorage.removeItem('currentUser');
              currentUser.delete();
              reject(err.message);
            },
            () => {
              // upload success
              storageRef.getDownloadURL().then((downloadURL) => {
                this.firebaseApp.database().ref('users/' + userID).set({
                  Title: formValue.Title,
                  FName: formValue.FName,
                  LName: formValue.LName,
                  Email: formValue.Email,
                  Address: formValue.Address,
                  imageURL: downloadURL
                }).then((innerResult) => {
                  resolve('save data successful');
                }).catch((err) => {
                  const currentUser = this.firebaseApp.auth().currentUser;
                  this.isUserLoggedIn.next(false);
                  localStorage.removeItem('currentUser');
                  currentUser.delete();
                  reject(err.message);
                }).catch((err) => {
                  const currentUser = this.firebaseApp.auth().currentUser;
                  this.isUserLoggedIn.next(false);
                  localStorage.removeItem('currentUser');
                  currentUser.delete();
                  reject(err.message);
                });
              })
                .catch((err) => {
                  reject(err.message)
                })
              //   .catch((err) => {
              //     reject(err.message);
              //   }).catch((err) => {
              //   reject(err.message);
              // }).catch((err)=>{
              //   reject(err);
              // })
            })
        }).catch((err)=>{
          reject(err);
      })
    })
  }

  LogIn(formValue: any): Promise<boolean> {
    return new Promise<any>((resolve, reject) => {
      this.firebaseApp.auth().signInWithEmailAndPassword(formValue.Email, formValue.Password)
        .then((result) => {
          this.isUserLoggedIn.next(true);
          localStorage.setItem('currentUser', JSON.stringify({
            userStatus: 'normal-user',
            user: result.user
          }));
          resolve(true);
        }).catch((error) => {
        reject(false);
      });
    });
  }

  AdminLogin(formValue: any): Promise<boolean> {
    return new Promise<any>((resolve, reject) => {
      this.firebaseApp.auth().signInWithEmailAndPassword(formValue.Email, formValue.Password)
        .then((result) => {
          this.firebaseApp.database().ref('admin/').once('value')
            .then((snapshot) => {
              const adminKey = Object.keys(snapshot.val())[0];
              if (adminKey === result.user.uid) {
                this.isAdminLoggedIn.next(true);
                localStorage.setItem('currentUser', JSON.stringify({
                  userStatus: 'admin',
                  user: result.user
                }));
                resolve(true);
              } else {
                this.SignOut();
                localStorage.removeItem('currentUser');
                this.isAdminLoggedIn.next(false);
                reject(false);
              }
            }).catch((error) => {
            this.isAdminLoggedIn.next(false);
            localStorage.removeItem('currentUser');
            reject(false);
          });
        }).catch((error) => {
        this.isAdminLoggedIn.next(false);
        localStorage.removeItem('currentUser');
        reject(false);
      });
    });
  }

  SignOut(): Promise<string> {
    return new Promise((resolve, reject) => {
      const currentUser = localStorage.getItem('currentUser');
      if (currentUser !== 'null' && currentUser !== null) {
        // logout user
        this.firebaseApp.auth().signOut().then(() => {
          localStorage.removeItem('currentUser');
          this.isUserLoggedIn.next(false);
          this.isAdminLoggedIn.next(false);
          resolve('signed out successfully!');
        }).catch(() => {
          reject('Signout Error: could not sign out.');
        })
      } else {
        // user is not logged in
        reject('Signout Error: user not logged in.');
      }
    });
  }
}
