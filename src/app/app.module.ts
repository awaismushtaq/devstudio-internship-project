import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

import {CourseModule} from "./modules/course/course.module";
import {ShopModule} from "./modules/shop/shop.module";
import {MailModule} from "./modules/mail/mail.module";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ApiRequestsModule} from "./modules/api-requests/api-requests.module";

import {MapComponent} from './components/map/map.component';

import {AgmCoreModule} from '@agm/core';
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";
import {AuthModule} from "./modules/auth/auth.module";
import {AdminModule} from "./modules/admin/admin.module";
import {StoreModule} from "./modules/store/store.module";
import {AuthPageGuard} from "./guards/auth-page.guard";
import {StoreGuard} from "./guards/store.guard";
import {AdminPageGuard} from "./guards/admin-page.guard";
import {NgxLoadersCssModule} from "ngx-loaders-css";


@NgModule({
  declarations: [
    AppComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CourseModule,
    ShopModule,
    MailModule,
    AuthModule,
    AdminModule,
    StoreModule,
    ApiRequestsModule,
    BrowserAnimationsModule,
    NgbModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBjET7vufGHnYur2Tg1jneuHbcrY4ejpOc'
    })
  ],
  providers: [
    AuthPageGuard,
    StoreGuard,
    AdminPageGuard
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

platformBrowserDynamic().bootstrapModule(AppModule);

