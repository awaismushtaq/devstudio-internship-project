import { Component, OnInit } from '@angular/core';
import {MouseEvent as AgmMouseEvent} from '@agm/core';
import {LocationMark} from "../../classes/LocationMark";
import {AllMessages} from "../../data/all-messages";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements OnInit {

  lat: number = 31.520369600000002;
  lng: number = 74.35874729999999;
  markers: LocationMark[] = [];

  // lat: number;
  // lng: number;
  // iconUrl = '../images/location-icon.png';
  // iconUrl = 'https://img.icons8.com/dusk/64/000000/user-location.png';

  constructor() {
    // console.log('in constructor');
    for (let i = 0; i < AllMessages.length; i++){
      // console.log(AllMessages[i].location);
      this.markers.push(AllMessages[i].location);
    }

    // if (navigator){
    //   navigator.geolocation.getCurrentPosition(pos => {
    //     this.lng  = +pos.coords.longitude;
    //     this.lat  = +pos.coords.latitude;
    //   });
    // }

    // this.markers.push({
    //   lat: this.lat,
    //   lng: this.lng,
    //   label: 'A',
    //   draggable: false
    // });
  }

  ngOnInit(): void {
  }

  markerClicked(label: string, i: number): void {
    alert(`clicked the marker: ${label || i}`);
  }


  // mapClicked($event: AgmMouseEvent): void {
  //   this.markers.push({
  //     lat: $event.coords.lat,
  //     lng: $event.coords.lng,
  //     draggable: true
  //   });
  // }
}

// just an interface for type safety
interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
