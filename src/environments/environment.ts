// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBULuBGbdB-fmOVF16SfFI1P3RdN2ebb2E",
    authDomain: "angular-authentication-c4c59.firebaseapp.com",
    databaseURL: "https://angular-authentication-c4c59.firebaseio.com",
    projectId: "angular-authentication-c4c59",
    storageBucket: "angular-authentication-c4c59.appspot.com",
    messagingSenderId: "940118687231",
    appId: "1:940118687231:web:338e389ddadf04321cbbca"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
