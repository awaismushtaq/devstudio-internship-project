export const APIURLs = {
  commentsAPI: 'http://jsonplaceholder.typicode.com/posts/1/comments',
  commentsAPIByParams: 'http://jsonplaceholder.typicode.com/posts',
  postsAPIPost: 'http://jsonplaceholder.typicode.com/posts',
  postsAPIManipulation: 'http://jsonplaceholder.typicode.com/posts/1',
  albumsAPI: 'http://jsonplaceholder.typicode.com/albums',
  photosAPI: 'http://jsonplaceholder.typicode.com/photos'
}
